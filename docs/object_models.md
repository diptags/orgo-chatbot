# Object Models

## Organisasi
 - nama : String
  (harus ada)
 - ketua : Peran
  (harus ada)
 - deskripsi : String
 - jadwal_grup : Jadwal_Grup

## Grup
 - parentGrup : Grup
  (harus ada, setidaknya Organisasi)
 - nama : String
  (harus ada) (misal Divisi xxx, Team xxx, Biro xxx) (identik)
 - anggota[] : Peran
 - ketua : Peran
  (harus ada)
 - proker[] : Proker
 - agenda[] : Agenda
 - childGrup[] : Grup
 - deskripsi : String
 - jadwal_grup : Jadwal_Grup

## Proker
 - isDone : bool (harus ada)
 - nama : String (harus ada)
 - deskripsi : String
 - jadwal[] : Jadwal (jangan lupa dapetin jadwal yg di agenda)
 - agenda[] : Agenda
 - proker[] : Proker

## Agenda
 - isDone : bool (harus ada)
 - nama : String (harus ada)
 - deskripsi : String
 - jadwal[] : Jadwal

## Orang
 - id : int
  (harus ada, identik)
 - Nama : String
  (harus ada)
 - peran[] : Peran
 - jadwal : Jadwal_orang

## Peran
 - Person
 - Jabatan

## Jabatan
 - peran : Peran
  (harus ada)
 - parent_jabatan : Jabatan
 - child_jabatan[] : Jabatan
 - authority : Authority

## Authority
 - bisa menambahkan ke grup x
 - bisa menghapus grup x
 - belum kepikiran lagi

## Jadwal_organisasi
 - main_jadwal[] : Jadwal
 - sub_jadwal[] : Jadwal
 - waktu : Time
 - interval_waktu : Time_something

## Jadwal_orang
 - orang : Orang
 - jadwal[] : Jadwal

## Jadwal
 - id : String
 - nama : String
 - interval_waktu : Interval_waktu
 - waktu : Time
 - tempat : Location
 - deskripsi : String