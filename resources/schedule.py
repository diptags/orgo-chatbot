import atexit
from datetime import datetime

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
from pytz import timezone

from resources.global_var import get_database_conn


def routine_job():
    from resources.global_var import set_database_conn
    import psycopg2
    from setting.db_config import DATABASE_URL
    set_database_conn(psycopg2.connect(DATABASE_URL))
    from resources.handler import remind_schedule
    remind_schedule()


def start_schedule():
    scheduler = BackgroundScheduler()
    now = datetime.now(timezone("Asia/Jakarta"))
    start_time = datetime(year=now.year, month=now.month, day=now.day, hour=20, minute=00)
    scheduler.add_job(func=routine_job,
                      trigger=IntervalTrigger(days=1, start_date=start_time,
                                              timezone=timezone("Asia/Jakarta")))
    scheduler.start()
    atexit.register(lambda: scheduler.shutdown())
    atexit.register(lambda: get_database_conn().close())
