from linebot.models import SourceGroup, SourceUser, SourceRoom, FlexSendMessage, TextSendMessage

from resources.database.announcement_db import delete_announcement_db, get_db_announcement
from resources.database.files_db import get_org_files
from resources.database.organization_db import insert_organisasi, get_organization_details, \
    set_anonymous_mode
from resources.database.schedule_db import add_org_schedule, get_db_schedule, update_schedule_db, \
    delete_schedule_db
from resources.database.session_db import get_session_data, update_session, clear_session_data
from resources.database.user_db import get_user_organizations, get_user_organization_ids
from resources.database.workplans_db import set_done_workplan, delete_workplan, get_workplan_name, \
    get_db_workplan, get_org_workplans
from resources.event_handler.file_handler import get_document
from resources.global_var import get_current_session, get_current_event
from resources.handler.announcements_handler import get_announcement
from resources.handler.organization_handler import create_organization
from resources.handler.schedule_handler import notify_new_schedule, get_schedule
from resources.handler.sender import reply
from resources.handler.workplan_handler import get_workplan
from resources.models import Schedule
from resources.modules.user_interface import berhasil_membuat_organisasi, get_workplan_ui, \
    get_schedule_ui, get_edit_schedule_ui, get_schedule_date_ui, get_edit_announcement_ui, \
    get_edit_workplan_ui, admin_orgo, konfirmasi_keluar, bot_keluar, siapakah_orgo, \
    get_organization_schedule_ui, lihat_proker_organisasi, lihat_pengumuman_organisasi, \
    lihat_dokumen_organisasi
from resources.help import feedbackOrgo, aturGrupOrgo, aturJadwalOrgo, aturPengumumanOrgo, \
    aturProkerOrgo, aturDokumenOrgo, infoLainOrgo, fiturTerjemahan, fiturCuaca, fiturGizi, \
    fiturKalkulator
from resources.setup import line_bot_api


def handle_postback():
    session = get_current_session()

    event = get_current_event()
    user_id = event.source.user_id
    if isinstance(event.source, SourceGroup):
        schedule_id = event.source.group_id
    elif isinstance(event.source, SourceUser):
        schedule_id = "private chat"
    elif isinstance(event.source, SourceRoom):
        schedule_id = "multi chat"


    postback_data = event.postback.data
    postback_split = event.postback.data.split("-")

    if session == "create-organization-stage-3":
        if postback_data == "Ya, Lanjutkan!":

            organization_detail = get_session_data()

            prosesTambah = insert_organisasi(event.source.group_id,
                                             organization_detail[0],
                                             organization_detail[1])
            if prosesTambah == "gagal mendaftarkan organisasi":
                reply(
                    "Mohon maaf, terjadi kesalahan sistem saat Orgo hendak menambahkan "
                    "grup/organisasi ! Cobalah kembali sesaat lagi")
            else:
                berhasil_membuat_organisasi(event)
            clear_session_data()
            update_session("default")

        elif postback_data == "Tidak":
            clear_session_data()
            update_session("default")
            create_organization()

    elif session == "create-schedule-stage-4":
        schedule_date =event.postback.params["datetime"]
        schedule_detail = get_session_data()
        jadwal_final = Schedule(name=schedule_detail[0],
                                descriptions=schedule_detail[1],
                                location=schedule_detail[2],
                                date=schedule_date,
                                id=-1)
        prosesTambah = add_org_schedule(jadwal_final, event.source.group_id)
        reply(prosesTambah)
        clear_session_data()
        update_session("default")
        notify_new_schedule(jadwal_final)

    elif "-".join(session.split("-")[0:3]) == "edit-schedule-date":
        id = session.split("-")[3]
        schedule_date =event.postback.params["datetime"]
        update_schedule_db("date", schedule_date, id)
        reply("Waktu jadwal telah diperbarui")
        update_session("default")

    elif session == "default":

        if len(postback_split) >= 3:
            command = "-".join(postback_split[0:2])
            arg3 = postback_split[2]
            if len(postback_split) >= 4:
                arg4 = postback_split[3]
        else:
            command = ""

        if command == "done-workplan":
            workplan_id = arg3
            workplan_name = get_workplan_name(workplan_id)
            set_done_workplan(workplan_id, "true")
            reply("Program kerja " + workplan_name + " telah tuntas")

        elif command == "notdone-workplan":
            workplan_id = arg3
            set_done_workplan(workplan_id, "false")
            workplan_name = get_workplan_name(workplan_id)
            reply("Program kerja " + workplan_name + " belum tuntas")

        elif command == "delete-workplan":
            workplan_id = arg3
            delete_workplan(workplan_id)
            workplan_name = get_workplan_name(workplan_id)
            reply("Program kerja " + workplan_name + " telah dihapus")

        elif command == "show-workplan":
            workplan_id = arg3
            workplan = get_db_workplan(workplan_id)
            if event.source.type == "group":
                editable = True
            elif event.source.type == "user":
                editable = False

            ui = get_workplan_ui(workplan, editable=editable)
            reply(ui)

        elif command == "show-schedule":
            schedule_id = arg3
            schedule = get_db_schedule(schedule_id)
            if event.source.type == "group":
                editable = True
            elif event.source.type == "user":
                editable = False

            ui = get_schedule_ui(schedule, editable=editable)
            line_bot_api.reply_message(event.reply_token, ui)

        elif command == "edit-schedule":
            attribute = arg3

            if attribute == "title":
                schedule_id = arg4
                update_session("edit-schedule-title-" + schedule_id)
                reply("Silakan masukan judul baru")

            elif attribute == "date":
                schedule_id = arg4
                update_session("edit-schedule-date-" + schedule_id)
                schedule_date_ui = get_schedule_date_ui("silakan masukan waktu baru")
                showUI = FlexSendMessage(alt_text="Tambahkan waktu Jadwal",contents=schedule_date_ui)
                line_bot_api.reply_message(event.reply_token, showUI)

            elif attribute == "location":
                schedule_id = arg4
                update_session("edit-schedule-location-" + schedule_id)
                reply("Silakan masukan tempat baru")

            elif attribute == "descriptions":
                schedule_id = arg4
                update_session("edit-schedule-descriptions-" + schedule_id)
                reply("Silakan masukan deskripsi baru")

            else:
                schedule_id = arg3
                schedule = get_db_schedule(schedule_id)
                ui = get_edit_schedule_ui(schedule)
                line_bot_api.reply_message(event.reply_token, ui)

        elif command == "edit-workplan":
            attribute = arg3

            if attribute == "name":
                workplan_id = arg4
                update_session("edit-workplan-name-" + workplan_id)
                reply("Silakan masukan nama program kerja baru")

            elif attribute == "pic":
                workplan_id = arg4
                update_session("edit-workplan-pic-" + workplan_id)
                reply("Silakan masukan penanggung jawab baru")

            elif attribute == "descriptions":
                workplan_id = arg4
                update_session("edit-workplan-descriptions-" + workplan_id)
                reply("Silakan masukan deskripsi baru")

            else:
                workplan_id = arg3
                workplan = get_db_workplan(workplan_id)
                ui = get_edit_workplan_ui(workplan)
                reply(ui)

        elif command == "edit-announcement":
            attribute = arg3

            if attribute == "title":
                announcement_id = arg4
                update_session("edit-announcement-title-" + announcement_id)
                reply("Silakan masukan judul baru")

            elif attribute == "text":
                announcement_id = arg4
                update_session("edit-announcement-text-" + announcement_id)
                reply("Silakan masukan deskripsi baru")

            else:
                announcement_id = arg3
                announcement = get_db_announcement(announcement_id)
                ui = get_edit_announcement_ui(announcement)
                line_bot_api.reply_message(event.reply_token, ui)

        elif command == "delete-schedule":
            schedule_id = arg3
            delete_schedule_db(schedule_id)
            reply("Jadwal berhasil dihapus")

        elif command == "delete-announcement":
            announcement_id = arg3
            print(announcement_id)
            delete_announcement_db(announcement_id)
            reply("Pengumuman berhasil dihapus")

        elif command == "start-anonymous":
            schedule_id = arg3
            group_name = get_organization_details(schedule_id).name
            set_anonymous_mode(group_id=schedule_id, mode_is_on=True)
            reply("Anda telah memasuki mode rahasia pada grup " + group_name
                  + "\n \n untuk keluar dari mode rahasia, silahkan ketik BATAL")
            update_session("anonymous-mode-"+schedule_id)

        elif command == "show-group":
            model = arg3
            group_id = arg4

            if model == "schedules":
                get_schedule(group_id)

            elif model == "announcements":
                get_announcement(group_id)

            elif model == "workplans":
                get_workplan(group_id)

            elif model == "documents":
                get_document(group_id)

    postback_split = event.postback.data.split("-")

    if postback_split[0] == 'fitur':

        if postback_split[1] == 'tentang':
            line_bot_api.reply_message(event.reply_token, admin_orgo())

        elif postback_split[1] == 'konfirmasi':
            if isinstance(event.source, SourceUser):
                reply("Mohon maaf, fitur ini hanya bisa dilakukan dalam Grup & Multichat. "
                          "Personal Chat (PM) tidak didukung, karena Orgo tidak bisa keluar dari "
                          "personal chat!")
            else:
                konfirmasi_keluar(event)

    elif postback_split[0] == 'bot':

        if postback_split[1] == 'keluar':  # Jika user menekan tombol YA untuk keluar
            bot_keluar(event)

        elif postback_split[1] == 'tidak-keluar':
            reply('Terima kasih banyak atas keputusan Anda untuk tidak mengeluarkan Orgo')

    elif postback_split[0] == 'bantuan':

        if postback_split[1] == 'siapa':
            line_bot_api.reply_message(event.reply_token, siapakah_orgo())

        elif postback_split[1] == 'feedback':
            reply(feedbackOrgo)

        elif postback_split[1] == 'atur':

            if postback_split[2] == 'grup':
                reply(aturGrupOrgo)

            elif postback_split[2] == 'jadwal':
                reply(aturJadwalOrgo)

            elif postback_split[2] == 'pengumuman':
                reply(aturPengumumanOrgo)

            elif postback_split[2] == 'proker':
                reply(aturProkerOrgo)

            elif postback_split[2] == 'dokumen':
                reply(aturDokumenOrgo)

        elif postback_split[1] == 'lainnya':
            reply(infoLainOrgo)

        elif postback_split[1] == 'terjemahan':
            reply(fiturTerjemahan)

        elif postback_split[1] == 'cuaca':
            reply(fiturCuaca)

        elif postback_split[1] == 'carigizi':
            reply(fiturGizi)

        elif postback_split[1] == 'kalkulator':
            reply(fiturKalkulator)


    # Returnnya berupa List yang isinya objek organisasi dari yang diikuti oleh user

    USER_ORGANIZATION = get_user_organizations(event.source.user_id)
    USER_ORGANIZATION_IDS = get_user_organization_ids(event.source.user_id)
    STORED_ORGANIZATION = None  # Ini untuk keperluan keep track organisasi mana yang mau dilihat detailnya

    if postback_split[:3] == ['lihat', 'jadwal', 'org']:
        STORED_ORGANIZATION = USER_ORGANIZATION[int(postback_split[-1])]
        list_jadwal = STORED_ORGANIZATION.schedules
        ui = get_organization_schedule_ui(event, list_jadwal, STORED_ORGANIZATION.name)
        reply(ui)

        STORED_ORGANIZATION = None

    elif postback_split[:3] == ['lihat', 'proker', 'org']:
        ORGANIZATION_ID = USER_ORGANIZATION_IDS[int(postback_split[-1])]
        workplans = get_org_workplans(ORGANIZATION_ID)
        organization_name = get_organization_details(ORGANIZATION_ID).name
        lihat_proker_organisasi(event, workplans, organization_name)

    elif postback_split[:3] == ['lihat', 'pengumuman', 'org']:
        STORED_ORGANIZATION = USER_ORGANIZATION[int(postback_split[-1])]
        list_pengumuman = STORED_ORGANIZATION.announcements
        lihat_pengumuman_organisasi(event, list_pengumuman, STORED_ORGANIZATION.name)
        STORED_ORGANIZATION = None

    elif postback_split[:3] == ['lihat', 'jabatan', 'org']:
        token = get_user_organization_ids(event.source.user_id)[int(postback_split[-1])]
        token_msg = "Token grup:\n" + token
        line_bot_api.reply_message(event.reply_token, TextSendMessage(text=token_msg))
        update_session("buat-profil")

    elif postback_split[:3] == ['lihat', 'dokumen', 'org']:
        STORED_ORGANIZATION = USER_ORGANIZATION[int(postback_split[-1])]
        ORGANIZATION_ID = USER_ORGANIZATION_IDS[int(postback_split[-1])]

        list_file = get_org_files(ORGANIZATION_ID)
        lihat_dokumen_organisasi(event, list_file, STORED_ORGANIZATION.name)
        STORED_ORGANIZATION = None

    elif postback_split[:3] == ['kirim', 'pesan', 'org']:
        ORGANIZATION_ID = USER_ORGANIZATION_IDS[int(postback_split[-1])]
        msg = "Jika Anda memiliki pesan yang hendak disampaikan untuk grup/organisasi ini (Dapat berupa pendapat, saran, kritik, atau lainnya), Anda dapat menyampaikannya dengan cara:\n" + "\n" + "Isi Pesan | Kode Token. Contoh:\n" + "Menurut saya, lebih bagus pembagian THR dipercepat | 012345678910\n" + "\n" + "Token untuk sesi ini :"
        line_bot_api.reply_message(event.reply_token,
                                   [TextSendMessage(text=msg), TextSendMessage(text=ORGANIZATION_ID)])

    elif event.postback.data == 'postback pilih tanggal':
        msg = "test tanggal berhasil " + event.postback.params['datetime']
        line_bot_api.reply_message(event.reply_token,
                                   [TextSendMessage(text=msg)])

    else:
        pass

