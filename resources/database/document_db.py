# coding=utf-8
"""
Interface untuk mengakses database dokumen.
"""
import psycopg2

from resources.database.organization_db import exist_organization
from resources.database.utilities import normalize_sql_varchar
from resources.models import Document
from setting.db_config import DATABASE_URL


def set_new_org_document(document_tuple: Document, organization_id: str):
    """
    menambahkan dokumen baru ke sebuah organisasi
    :param document_tuple: instance dokumen baru yang akan di masukkan
    :param organization_id: id organisasi yang akan dimasukkan dokumen baru
    :return:
    """
    if not exist_organization(organization_id):
        print("organization not registered")
        return "organisasi belum terdaftar untuk grup ini"

    conn = None

    try:
        conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()

        print("inserting document data to organisasi_jadwal table ...")
        cur.execute(
            """
            INSERT INTO organization_document (organization_id,
                                               document_name,
                                               document_exp,
                                               document_descriptions,
                                               document_data)
            VALUES (%s,%s,%s,%s,%s)
            """, (organization_id,
                  document_tuple.name,
                  document_tuple.exp,
                  document_tuple.descriptions,
                  psycopg2.Binary(document_tuple.data)))

        print("committing the changes to the database...")
        conn.commit()

        print("closing communication with the database...")
        cur.close()

    except psycopg2.DatabaseError as error:
        print("Found error")
        print(error)
        return "gagal memasukkan dokumen"

    finally:
        if conn is not None:
            print("Closing connection...")
            conn.close()
    print("End inserting...")

    return "dokumen berhasil ditambahkan"


def get_org_documents(organization_id: str):
    """
    Mendapatkan semua dokumen organisasi
    :param organization_id: id organisasi
    :return: list dari named-tuple semua dokumen organisasi
    """
    documents = []

    conn = None
    print("\nStart getting organization documents...")
    try:
        conn = psycopg2.connect(DATABASE_URL)
        cur = conn.cursor()

        print("Getting organization documents from organization_document table...")

        cur.execute(
            """
            SELECT organization_id,
                   document_name,
                   document_exp,
                   document_descriptions,
                   document_data
            FROM organization_document
            WHERE organization_id = %s
            """, (organization_id,))

        cur.fetchone()

        rows = cur.fetchall()
        for row in rows:
            document_name = normalize_sql_varchar(row[0], "name")
            document_exp = normalize_sql_varchar(row[1], "kadaluarsa")
            document_descriptions = normalize_sql_varchar(row[2], "deskripsi")
            document_data = open(document_name, 'wb').write(row[3])
            document = Document(name=document_name,
                                exp=document_exp,
                                descriptions=document_descriptions,
                                data=document_data)
            documents.append(document)

        print("Closing communication with the database")
        cur.close()

    except psycopg2.DatabaseError as error:
        print("Found Error")
        print(error)
        return True

    finally:
        if conn is not None:
            print("Closing connection")
            conn.close()
        print("End getting organization document")

    return documents
