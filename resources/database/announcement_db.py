# coding=utf-8
"""
Interface untuk mengakses database pengumuman.
"""
import psycopg2

from resources.global_var import get_database_conn
from resources.models import Announcement
from resources.setup import database_url


def add_org_announcement(announcement_tuple: Announcement, organization_id: str):
    """
    Menambahkan pengumuman baru ke sebuah organisasi.
    :param announcement_tuple: data pengumuman baru yang akan di masukkan
    :param organization_id: id organisasi yang akan dimasukkan pengumuman baru
    :return: status keberhasilan penambahan pengumuman baru
    """
    conn = get_database_conn()
    cur = conn.cursor()

    cur.execute(
        """
        INSERT INTO organization_announcements (organization_id,
                                                announcement_title,
                                                announcement_text)
        VALUES (%s,%s,%s)
        """, (organization_id,
              announcement_tuple.title,
              announcement_tuple.text))

    conn.commit()
    cur.close()
    return "Terima kasih, pengumuman berhasil dibuat"


def get_org_announcements(organization_id: str):
    """
    Mendapatkan semua pengumuman pada sebuah organisasi.
    :param organization_id: id organisasi yang akan didapatkan pengumuman2 nya
    :return: list of all announcement data
    """
    announcements_list = []

    conn = get_database_conn()
    cur = conn.cursor()

    # print("Getting organization announcements from organization_announcements table ...")
    cur.execute(
        """
        SELECT announcement_id,
                announcement_title,
                announcement_text
        FROM organization_announcements
        WHERE organization_id = %s
        """, (organization_id,))


    rows = cur.fetchall()
    for row in rows:
        announcement_id = row[0]
        announcement_title = row[1]
        announcement_text = row[2]

        announcement = Announcement(id=announcement_id,
                                    title=announcement_title,
                                    text=announcement_text)
        announcements_list.append(announcement)

    cur.close()


    announcements_list.reverse()

    return announcements_list


def modify_org_announcement(announcement_id: str, new_announcement_tuple: Announcement):
    """
    Mengubah sebuah pengumuman.
    :param announcement_id: id pengumuman yang ingin di ubah
    :param new_announcement_tuple: pengumuman baru yang akan di masukkan
    :return: berhasil atau tidaknya perubahan pengumuman
    """
    conn = None

    try:
        conn = psycopg2.connect(database_url)
        cur = conn.cursor()

        cur.execute(
            """
            UPDATE organization_announcements
            SET announcement_title = %s,
                announcement_text = %s
            WHERE announcement_id = %s
            """, (new_announcement_tuple.title,
                  new_announcement_tuple.text,
                  announcement_id))

        conn.commit()
        cur.close()

    except psycopg2.DatabaseError as error:
        # print("Found error")
        # print(error)
        return "Terjadi kesalahan saat Orgo hendak mengubah pengumuman, silakan coba kembali " \
               "beberapa saat lagi"

    finally:
        if conn is not None:
            # print("Closing connection...")
            conn.close()
    # print("End modifying...")

    return "Terima kasih, pengumuman berhasil diubah"

def delete_announcement_db(announcement_id: str):
    conn = get_database_conn()
    cur = conn.cursor()

    cur.execute(
        """
        DELETE FROM organization_announcements
        WHERE announcement_id = %s
        """,(announcement_id,))
    conn.commit()
    cur.close()

def get_db_announcement(announcement_id):
    conn = get_database_conn()
    cur = conn.cursor()

    cur.execute(
        """
        SELECT announcement_title, announcement_text
        FROM organization_announcements
        WHERE announcement_id = %s
        """, (announcement_id,))
    announcement = cur.fetchone()
    announcement = Announcement(title=announcement[0],
                                text=announcement[1],
                                id=announcement_id)
    cur.close()
    return announcement


def update_announcement_db(attribute: str, value: str, id: str):
    attributes_list = ["title", "text"]

    if attribute not in attributes_list:
        # print("Wrong Attribute Error")
        return

    conn = get_database_conn()
    cur = conn.cursor()

    sql = ("UPDATE organization_announcements "
           + "SET announcement_" + attribute + " = '" + value + "'"
           + "WHERE announcement_id = " + id)

    cur.execute(sql)
    conn.commit()
    cur.close()



# if __name__ == '__main__':
#     ANNOUNCEMENT = get_org_announcements("Cc700dd015e063ff98c86115bed7dd906")


