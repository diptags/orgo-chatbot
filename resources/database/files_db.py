import psycopg2

from resources.database.utilities import normalize_sql_varchar
from resources.models import File
from resources.setup import database_url


def get_org_files(organization_id):
    """
    Mendapatkan semua files organisasi
    :param organization_id: id organisasi
    :return: list dari named-tuple file
    """

    files = []

    conn = None
    print("\nStart getting organization workplans...")
    try:
        conn = psycopg2.connect(database_url)
        cur = conn.cursor()

        print("Getting organization proker")
        cur.execute(
            """
            SELECT file_name, file_url
            FROM organization_files
            WHERE organization_id = %s
            """, (organization_id,))

        # cur.fetchone()

        rows = cur.fetchall()
        for row in rows:
            file_name = normalize_sql_varchar(row[0], "name")
            file_url = normalize_sql_varchar(row[1], "url")
            file = File(name=file_name,
                        url=file_url)
            files.append(file)

        print("Closing communication with the database")
        cur.close()

    except psycopg2.DatabaseError as error:
        print("Found Error")
        print(error)
        return True

    finally:
        if conn is not None:
            print("Closing connection")
            conn.close()
        print("End getting organization file")

    return files


def add_org_file(file: File, organization_id: str):
    """
    Menambahkan file baru ke sebuah organisasi
    :param file: named-tuple file
    :param organization_id: id organisasi
    :return: berhasil tidaknya penambahan file
    """

    conn = None

    try:
        conn = psycopg2.connect(database_url)
        cur = conn.cursor()

        print("inserting file data to organization_files table ...")
        cur.execute(
            """
            INSERT INTO organization_files (organization_id,
                                                file_name,
                                                file_url)
            VALUES (%s,%s,%s);
            """, (organization_id,
                  file.name,
                  file.url))

        conn.commit()
        cur.close()

    except psycopg2.DatabaseError as error:
        print("Found error")
        print(error)
        return "Terjadi kesalahan saat menambahkan dokumen, silakan coba kembali beberapa saat lagi"

    finally:
        if conn is not None:
            print("Closing connection...")
            conn.close()
    print("End inserting...")

    return "OK"
