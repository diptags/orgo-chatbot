# coding=utf-8
"""
Interface untuk mengakses database organisasi
"""
import collections

import psycopg2

from resources.database.announcement_db import get_org_announcements
from resources.database.schedule_db import get_org_schedules
from resources.database.utilities import normalize_sql_tuple
from resources.database.utilities import normalize_sql_varchar
from resources.errors.db_errors import UserHasNoProfileError
from resources.global_var import get_database_conn
from resources.models import Group
from resources.models import User

OrganizationDetails = collections.namedtuple("OrganizationDetails", ["name", "descriptions"])


def exist_organization(organization_id):
    """
    Check if organization with id in param exist
    :param organization_id: Line ID of Group
    :return boolean: existence of organization
    """

    # conn = None
    is_exist = True
    try:

        conn = get_database_conn()
        cur = conn.cursor()

        cur.execute("""
                    SELECT organization_name 
                    FROM organizations
                    WHERE organization_id = %s ;
                    """, (organization_id,))

        row = cur.fetchone()

        if row is None:
            is_exist = False

        cur.close()

    except (psycopg2.DatabaseError, psycopg2.InterfaceError) as error:
        return True

    return is_exist


def insert_organisasi(organization_id: str,
                      organization_name: str,
                      organization_descriptions=""):
    """
    Memasukkan orgaisasi baru ke database
    :param organization_id: id organisasi yang akan di masukkan
    :param organization_name: nama organisasi yang akan di masukkan
    :param organization_descriptions: deskripsi organisasi yang akan di masukkan
    :return: keberhasilan memasukkan organisasi ke database
    """

    if exist_organization(organization_id):
        return "grup ini sudah terdaftar sebagai organisasi"

    # conn = None

    try:
        conn = get_database_conn()
        cur = conn.cursor()

        cur.execute(
            """
            INSERT INTO organizations (organization_id,
                                       organization_name,
                                       organization_descriptions)
            VALUES(%s,%s,%s);
            """, (organization_id,
                  organization_name,
                  organization_descriptions,))

        conn.commit()
        cur.close()

    except(psycopg2.DatabaseError, psycopg2.InterfaceError) as error:
        return "gagal mendaftarkan organisasi"


    return "organisasi berhasil di daftarkan"


def get_all_organization_id():
    conn = get_database_conn()
    cur = conn.cursor()

    organizations_id = []
    cur.execute(
        """
        SELECT organization_id
        FROM organizations
        """
    )
    rows = cur.fetchall()
    cur.close()
    if rows is not None:
        for row in rows:
            organization_id = row[0]
            organizations_id.append(organization_id)
    return organizations_id


def get_organization_details(organization_id: str):
    """
    Mendapatkan detail organisasi
    :param organization_id:
    :return: organization name, organization descriptions
    """

    # conn = None
    organization_name = ""
    organization_descriptions = ""
    try:

        conn = get_database_conn()
        cur = conn.cursor()


        cur.execute(
            """
            SELECT organization_name, organization_descriptions
            FROM organizations
            WHERE organization_id = %s
            """, (organization_id,))

        row = cur.fetchone()

        organization_name = normalize_sql_varchar(row[0])
        organization_descriptions = normalize_sql_varchar(row[1], "Deskripsi")

        cur.close()

    except(psycopg2.DatabaseError, psycopg2.InterfaceError) as error:
        return True


    organization_details = OrganizationDetails(name=organization_name,
                                               descriptions=organization_descriptions)
    return organization_details


def get_organization(organization_id: str):
    """
    Mendapatkan objek organisasi
    :param organization_id: id organisasi
    :return: named tuple organisasi
    """
    organization_details = get_organization_details(organization_id)
    organization_name = organization_details.name
    organization_descriptions = organization_details.descriptions

    organization_schedules = get_org_schedules(organization_id)
    organization_announcements = get_org_announcements(organization_id)

    organization = Group(name=organization_name,
                         descriptions=organization_descriptions,
                         schedules=organization_schedules,
                         announcements=organization_announcements,
                         id=organization_id)

    return organization


def get_members_id(organization_id: str):
    members_id = []
    # # conn = None
    conn = get_database_conn()
    try:

        # conn = get_database_conn()
        cur = conn.cursor()


        cur.execute(
            """
            SELECT user_id
            FROM organization_members
            WHERE organization_id = %s
            """, (organization_id,))

        rows = cur.fetchall()

        for row in rows:
            row = row[0]
            members_id.append(row)

        cur.close()

    except(psycopg2.DatabaseError, psycopg2.InterfaceError) as error:
        return True

    return members_id


def get_each_member_profile(user_id: str):
    """
    Mendapatkan profil pengguna
    :param user_id: id pengguna
    :return: named-tuple data pengguna
    """

    # conn = None

    try:

        conn = get_database_conn()
        cur = conn.cursor()

        cur.execute(
            """
            SELECT (user_name, user_bod, user_skill)
            FROM users
            WHERE user_id = %s
            """, (user_id,))

        row = cur.fetchone()
        if row is None:
            raise UserHasNoProfileError
        row = normalize_sql_tuple(row)
        user_name = normalize_sql_varchar(row[0])
        user_bod = normalize_sql_varchar(row[1])
        user_skill = normalize_sql_varchar(row[2])

        user_profile = User(id=user_id, name=user_name, bod=user_bod, skill=user_skill)

        cur.close()

    except(psycopg2.DatabaseError, psycopg2.InterfaceError) as error:
        return None

    return user_profile


def get_members_profile(organization_id: str):
    members_profile = []
    members_id = get_members_id(organization_id)
    for member_id in members_id:
        member_profile = get_each_member_profile(member_id)
        members_profile.append(member_profile)
    return member_profile

def get_all_user_organizations_id(user_id: str):
    conn = get_database_conn()
    cur = conn.cursor()

    organizations_id = []
    cur.execute(
        """
        SELECT organization_id
        FROM organization_members
        WHERE user_id = %s
        """, (user_id,)
    )
    rows = cur.fetchall()
    cur.close()
    if rows is not None:
        for row in rows:
            organization_id = row[0]
            organizations_id.append(organization_id)
    return organizations_id

def get_anonymous_mode(group_id: str):
    conn = get_database_conn()
    cur = conn.cursor()

    cur.execute(
        """
        SELECT anonymous_mode
        FROM organizations
        WHERE organization_id = %s
        """, (group_id,)
    )
    mode_is_on = cur.fetchone()

    cur.close()
    return mode_is_on

def set_anonymous_mode(group_id: str, mode_is_on: bool):
    conn = get_database_conn()
    cur = conn.cursor()

    cur.execute(
        """
        UPDATE organizations
        SET anonymous_mode = %s
        WHERE organization_id = %s
        """, (mode_is_on, group_id)
    )
    conn.commit()
    cur.close()

def get_anonymous_members_id(group_id):
    conn = get_database_conn()
    cur = conn.cursor()

    anonymous_members_id = []

    members_id = get_members_id(group_id)
    for member_id in members_id:
        cur.execute(
            """
            SELECT session_name
                FROM sessions
                WHERE organization_id = %s AND user_id = %s
            """, ('private_chat', member_id)
        )
        session_name = cur.fetchone()[0]
        if session_name == "anonymous-mode-" + group_id:
            anonymous_members_id.append(member_id)
    cur.close()

    return anonymous_members_id
