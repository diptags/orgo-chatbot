import psycopg2

from resources.setup import database_url

variables = {'CURRENT_EVENT': None,
             'DATABASE_CONN': None,
             'CURRENT_SESSION': None
             }


def get_current_session():
    return variables['CURRENT_SESSION']


def set_session(session):
    variables['CURRENT_SESSION'] = session


def get_current_event():
    return variables['CURRENT_EVENT']


def set_event(event):
    variables['CURRENT_EVENT'] = event


def set_database_conn(database_conn):
    variables['DATABASE_CONN'] = database_conn


def get_database_conn():
    conn = variables['DATABASE_CONN']
    try:
        cur = conn.cursor()
        cur.close()
    except:
        conn = psycopg2.connect(database_url)
        set_database_conn(conn)
    return conn
