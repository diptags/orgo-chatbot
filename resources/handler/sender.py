import linebot
from linebot.models import TextSendMessage

from resources.database.organization_db import get_anonymous_members_id
from resources.global_var import get_current_event
from resources.setup import line_bot_api

event = None


def reply(message):
    """
    Fungsi untuk memberikan respon berupa TEKS

    Method reply_message (penamaan udah dari sananya) untuk memberikan balasan
    event.reply_token (penamaan udah dari sananya) menyatakan token dari event... event balas pesan
    TextSendMessage (penamaan udah dari sananya), menyatakan balasan yang dikirim berupa pesan teks
    (text = pesan) maksudnya adalah, pesan apa yang mau dibalas

    Parameternya adalah reply_message(TOKEN, OBJEK yang mau dibalas ke user)
    Misalnya bot mau membalas dengan sebuah gambar, maka jadi
    reply_message(event.reply_token, ImageSendMessage(link gambar))

    Atau misalnya mau menampilkan sebuah button (yang komponennya disimpan dalam variabel "tombol" misalnya)
    reply_message(event.reply_token, tombol)

    Penjelasan syntax: https://github.com/line/line-bot-sdk-python

    :param pesan: yang mau di balas
    """
    event = get_current_event()

    print("=== === === MESSAGE OUT === === ===")
    print(message)
    print("=== === === === === === === === ===")

    if isinstance(message, str):
        message = TextSendMessage(text=message)
    line_bot_api.reply_message(event.reply_token, message)


def send_message_to_developer(message: str):
    reply("Terima Kasih, Pesan telah Orgo kirimkan!")
    balasan_dev = 'Halo, kita dapat pesan dari pengguna lho: \n' + '\n' + message
    try:
        line_bot_api.push_message('C612771c5e4520f53492cc7eca882d571',
                                  TextSendMessage(text=balasan_dev))
    except linebot.exceptions.LineBotApiError:
        pass

def anonymous_message(message: str, receiver_id):
    reply("Terima kasih, pesan Anda telah Orgo kirimkan!")
    line_bot_api.push_message(receiver_id, TextSendMessage(text=message))

def send_message_to_anonymous_member(group_id: str):
    event = get_current_event()
    message = event.message.text

    try:
        sender = line_bot_api.get_profile(event.source.user_id).display_name
        message = sender + " :\n" + message
        anonymous_members_id = get_anonymous_members_id(group_id)
        for anonymous_member_id in anonymous_members_id:
            line_bot_api.push_message(anonymous_member_id, TextSendMessage(text=message))

    except linebot.exceptions.LineBotApiError:
        print("Fitur pesan rahasia tidak dapat digunakan, Pengguna belum add Orgo as friend")
