import os.path
import sys

import linebot

from resources.database.session_db import update_session, append_session_data
from resources.handler.basic_handler import basic_check
from resources.handler.sender import reply
from resources.models import Schedule
from resources.setup import line_bot_api

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")

from linebot.models import SourceGroup, SourceRoom, FlexSendMessage, TextSendMessage

from resources.help import ERROR_NOT_JOINED_ANY_ORG, ERROR_NON_GROUP
from resources.database.user_db import get_user_organizations, get_all_users_id
from resources.database.schedule_db import get_org_schedules, show_all_schedule_in_string, \
    filter_tomorrow, get_user_schedules, filter_future
from resources.database.organization_db import get_organization_details, get_members_id
from resources.global_var import get_current_event
from resources.modules.user_interface import get_schedule_date_ui, brief_schedules_ui

CREATE_NEW_SCHEDULE_FORMAT = ("Silakan masukkan detail jadwal dengan format:\n" +
                              "Nama Jadwal | Lokasi | Waktu | Deskripsi\n" + "\n" +
                              "Contoh:\n" +
                              "Rapat Mingguan | Aula Terapung | Senin, 18.00 WIB | Rapat wajib "
                              "dihadiri atau dianggap mengundurkan diri\n" + "\n" +
                              "Ketikkan BATAL untuk membatalkan aksi ini")

PLEASE_INSERT_SCHEDULE_TITLE = (
        "Silakan masukkan nama jadwal yang akan di daftarkan\n"
        + "\n"
        + "Ketikkan BATAL untuk membatalkan aksi ini")

PLEASE_INSERT_SCHEDULE_DESCRIPTIONS = (
        "Silakan masukkan deskripsi jadwal\n"
        + "\n"
        + "Ketikkan BATAL untuk membatalkan aksi ini")
PLEASE_INSERT_SCHEDULE_LOCATION = (
        "Silakan masukkan lokasi jadwal dengan cara mengetikkan lokasi "
        "tersebut, atau mengirim lokasi melalui fitur lokasi line\n "
        + "\n"
        + "Ketikkan BATAL untuk membatalkan aksi ini")
PLEASE_INSERT_SCHEDULE_DATE = (
        "Silakan masukkan waktu jadwal tersebut akan dilaksanakan dengan menekan tombol di bawah ini\n"
        + "\n"
        + "Ketikkan BATAL untuk membatalkan aksi ini")


@basic_check
def create_schedule():
    event = get_current_event()
    if isinstance(event.source, SourceGroup):
        reply(PLEASE_INSERT_SCHEDULE_TITLE)
        update_session("create-schedule-stage-1")
    else:
        reply(ERROR_NON_GROUP)

def create_schedule_stage_1(schedule_name):
    append_session_data(schedule_name)
    reply(PLEASE_INSERT_SCHEDULE_DESCRIPTIONS)
    update_session("create-schedule-stage-2")


def create_schedule_stage_2(schedule_descriptions):
    append_session_data(schedule_descriptions)
    reply(PLEASE_INSERT_SCHEDULE_LOCATION)
    update_session("create-schedule-stage-3")



def create_schedule_stage_3(schedule_location):
    event = get_current_event()
    append_session_data(schedule_location)
    schedule_date_ui = get_schedule_date_ui(PLEASE_INSERT_SCHEDULE_DATE)

    showUI = FlexSendMessage(alt_text="Tambahkan waktu Jadwal",
                             contents=schedule_date_ui)

    line_bot_api.reply_message(event.reply_token, showUI)
    update_session("create-schedule-stage-4")

def notify_new_schedule(schedule: Schedule):
    event = get_current_event()
    group_id = event.source.group_id
    organization_name = get_organization_details(group_id).name

    schedule_string = ("Ada Jadwal Baru!" + "\n"
                       + "\n"
                       + "Organisasi : " + organization_name + "\n"
                       + "Judul : " + schedule.name + "\n"
                       + "Tempat : " + schedule.location + "\n"
                       + "Waktu : " + schedule.date + "\n"
                       + "\n"
                       + "Untuk lebih detailnya, bisa dilihat dengan mengetik /lihat_jadwal"
                       )

    members_id = get_members_id(group_id)
    for member_id in members_id:
        line_bot_api.push_message(member_id, TextSendMessage(text=schedule_string))



@basic_check
def get_schedule(group_id=None):
    event = get_current_event()

    if group_id is not None:
        schedules = get_org_schedules(group_id)
        schedules = filter_future(schedules)
        if not schedules:
            reply("Organisasi belum memiliki jadwal. Silakan tambahkan terlebih dahulu")
        else:
            if len(schedules) == 0:
                reply("Mohon maaf Belum ada Jadwal Tersedia")
            else:
                schedules.sort()
                ui = brief_schedules_ui(schedules, "Jadwal Grup")
                reply(ui)

    elif isinstance(event.source, SourceGroup):
        group_id = event.source.group_id
        schedules = get_org_schedules(group_id)
        schedules = filter_future(schedules)
        if not schedules:
            reply("Organisasi belum memiliki jadwal. Silakan tambahkan terlebih dahulu")
            return
        schedules.sort()
        ui = brief_schedules_ui(schedules, "Jadwal Grup")
        reply(ui)

    else:
        try:
            user_id = event.source.user_id
            organizations = get_user_organizations(user_id)
            if len(organizations) == 0:
                reply(ERROR_NOT_JOINED_ANY_ORG)
            elif len(organizations) > 20:
                reply("Mohon maaf, jumlah organisasi Anda melebihi batas (20)")
            else:
                schedules = get_user_schedules((user_id))
                if schedules == [] or len(schedules) == 0:
                    reply("Anda belum memiliki jadwal")
                else:
                    schedules = filter_future(schedules)
                    schedules.sort()
                    ui = brief_schedules_ui(schedules, "Jadwal Anda")
                    line_bot_api.reply_message(event.reply_token, ui)
                    if isinstance(event.source, SourceRoom):
                        reply("Silakan cek personal chat Anda")
        except linebot.exceptions.LineBotApiError:
            reply(
                "Mohon tambahkan Orgo sebagai teman dulu ya :) Nanti Orgo tidak dapat membalas "
                "pesan")


def remind_schedule():
    users_id = get_all_users_id()
    for user_id in users_id:
        schedules = filter_tomorrow(get_user_schedules(user_id))
        if schedules != []:
            schedules.sort()
            schedules = show_all_schedule_in_string(schedules)
            line_bot_api.push_message(user_id, TextSendMessage(text=schedules))
