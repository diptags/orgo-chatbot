''' File ini isinya adalah kumpulan method yang digunakan untuk menampilkan sesuatu ke user
Misalnya button, carousel, confirmation , dan lainnya (supaya tidak menumpuk di Main)

Untuk fitur sampingan, biarkan saja apa adanya, sedangkan mungkin berikutnya kalau ingin menambahkan sesuatu yang ditampilkan ke user
bisa diletakkan di sini'''

import os.path
import sys

import linebot
from linebot.models import DatetimePickerAction

from resources.database.utilities import beautify_db_datetime_format
from resources.database.workplans_db import workplan_bool_to_string, get_workplans_progress
from resources.global_var import get_current_event
from resources.handler.sender import reply
from resources.models import Workplan, Schedule, Announcement
from resources.setup import line_bot_api

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")
from linebot.models import (TextSendMessage, TemplateSendMessage,
                            RichMenu, RichMenuSize, RichMenuArea, RichMenuBounds,
                            PostbackAction, MessageAction, SourceGroup, SourceRoom,
                            ConfirmTemplate, FlexSendMessage, BubbleContainer,
                            ImageComponent, BoxComponent, TextComponent, ButtonComponent,
                            IconComponent,
                            SeparatorComponent, CarouselContainer)

from resources.modules.pindah_server_gambar import upload_image

WARNA_BIRU = '#1976d3'
WARNA_ORANGE = '#f67c01'
WARNA_MERAH = '#be292b'
WARNA_HITAM = '#393233'
WARNA_HIJAU = '#41AD4A'

''' --------------------- UNTUK RICH MENU ---------------------------------- '''


def menu_bar_buat(user_id):
    ''' Fungsi mengolah aset dari rich menu (Gambar, Link, Dan lainnya) '''

    MenuAwal = RichMenu(
        size=RichMenuSize(width=2500, height=1686),
        selected=False,
        name="Menu Awal Orgo",
        chat_bar_text="Menu Orgo",
        areas=[
            RichMenuArea(
                bounds=RichMenuBounds(x=0, y=9, width=830, height=843),
                action=MessageAction(label='Pusat Bantuan', text='/bantuan')),

            RichMenuArea(
                bounds=RichMenuBounds(x=831, y=9, width=840, height=843),
                action=MessageAction(label='Lihat Profil', text='/lihat_profil')),

            RichMenuArea(
                bounds=RichMenuBounds(x=1672, y=9, width=830, height=843),
                action=MessageAction(label='Menu Orgo', text='/menu')),

            RichMenuArea(
                bounds=RichMenuBounds(x=0, y=849, width=830, height=1686),
                action=MessageAction(label='Lihat Jadwal', text='/lihat_jadwal')),

            RichMenuArea(
                bounds=RichMenuBounds(x=831, y=849, width=840, height=1686),
                action=MessageAction(label='Lihat Organisasi', text='/lihat_semua')),

            RichMenuArea(
                bounds=RichMenuBounds(x=1672, y=849, width=830, height=1686),
                action=MessageAction(label='Lihat Pengumuman', text='/lihat_pengumuman')),

        ]
    )
    rich_menu = line_bot_api.create_rich_menu(rich_menu=MenuAwal)
    # print("Memulai membuat Rich Menu")
    # print("Mengupload gambar...")

    with open('controller.jpg', 'rb') as f:
        line_bot_api.set_rich_menu_image(rich_menu, 'image/jpeg', f)

    # print("Menghubungkan rich menu ke user")
    line_bot_api.link_rich_menu_to_user(user_id, rich_menu)
    # print("Rich menu berhasil dibuat, PROSES BERHENTI")


def menu_bar(user_id):
    ''' Fungsi mengolah Rich Menu '''
    ''' Maaf terpaksa pakai NESTED TRY-EXCEPT, Fungsinya memaksa begini '''

    try:
        line_bot_api.get_rich_menu_id_of_user(user_id)
    except linebot.exceptions.LineBotApiError:
        # print("Rich menu tidak ditemukan")
        # print("Saatnya membuat rich menu baru")
        try:
            # print("Memeriksa sebelum membuat rich menu")
            menu_bar_buat(user_id)
        except linebot.exceptions.LineBotApiError:
            print("Mencapai 1000 Rich Menu, Tidak bisa membuat Rich Menu lagi")


''' ---------------- UNTUK PUSAT BANTUAN (RICH MENU KIRI) ----------------------------------- '''


def get_bantuan(event):
    bubble_1 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1531760652/bantuan-bg.png',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                TextComponent(text='PUSAT BANTUAN ORGO', weight='bold', size='sm',
                              color=WARNA_BIRU),
                TextComponent(
                    text='Ingin mempelajari fitur Orgo ?\n' + 'Silakan pilih salah satu menu di bawah ini',
                    weight='bold', size='xs', color='#aaaaaa', wrap=True),
                SeparatorComponent(),
                TextComponent(text='MEMANFAATKAN ORGO', weight='bold', size='sm', color=WARNA_BIRU,
                              align='center'),
                SeparatorComponent(),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_BIRU,
                    action=PostbackAction(label='Mengatur Grup', data='bantuan-atur-grup')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Mengatur Jadwal', data='bantuan-atur-jadwal')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Mengatur Pengumuman',
                                          data='bantuan-atur-pengumuman')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Mengatur Proker', data='bantuan-atur-proker')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Mengatur Dokumen', data='bantuan-atur-dokumen')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Tambahan Informasi', data='bantuan-lainnya')),
                SeparatorComponent()
            ]
        )
    )

    bubble_2 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1531760652/bantuan-bg.png',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                TextComponent(text='PUSAT BANTUAN ORGO', weight='bold', size='sm',
                              color=WARNA_ORANGE),
                TextComponent(
                    text='Apakah Anda belum mengenal Orgo ?\n' + 'Silakan pilih salah satu menu di bawah ini',
                    weight='bold', size='xs', color='#aaaaaa', wrap=True),
                SeparatorComponent(),
                TextComponent(text='MENGENAL ORGO', weight='bold', size='sm', color=WARNA_ORANGE,
                              align='center'),
                SeparatorComponent(),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_ORANGE,
                    action=MessageAction(label='MENU UTAMA', text='/menu')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Siapakah Orgo ?', data='bantuan-siapa-orgo')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Admin Orgo', data='fitur-tentang')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Kirim Umpan Balik', data='bantuan-feedback')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Keluarkan Orgo', data='fitur-konfirmasi-keluar')),
                SeparatorComponent()
            ]
        )
    )

    bubble_3 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1531760652/bantuan-bg.png',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                TextComponent(text='PUSAT BANTUAN ORGO', weight='bold', size='sm',
                              color=WARNA_MERAH),
                TextComponent(
                    text='Orgo juga punya fitur lain lho!\n' + 'Silakan pilih salah satu menu di bawah ini',
                    weight='bold', size='xs', color='#aaaaaa', wrap=True),
                SeparatorComponent(),
                TextComponent(text='FITUR LAIN ORGO', weight='bold', size='sm', color=WARNA_MERAH,
                              align='center'),
                SeparatorComponent(),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_MERAH,
                    action=PostbackAction(label='Terjemahan Bahasa', data='bantuan-terjemahan')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Peramalan Cuaca', data='bantuan-cuaca')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Gizi Hidangan', data='bantuan-carigizi')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Kalkulator', data='bantuan-kalkulator')),
                SeparatorComponent()
            ]
        )
    )

    UI = CarouselContainer(
        contents=[bubble_1, bubble_2, bubble_3]
    )

    showUI = FlexSendMessage(alt_text="Pusat Bantuan Orgo", contents=UI)
    line_bot_api.reply_message(event.reply_token, showUI)


def get_schedule_date_ui(please_insert_date_string: str):
    bubble_1 = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                TextComponent(
                    text=please_insert_date_string,
                    weight='bold',
                    size='xs',
                    color=WARNA_HITAM,
                    wrap=True),
                SeparatorComponent(),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_BIRU,
                    action=DatetimePickerAction(
                        label="Tambahkan Waktu",
                        data="create-schedule-stage-4",
                        mode="datetime")
                )
            ]
        )
    )

    UI = CarouselContainer(
        contents=[bubble_1]
    )

    return UI


# ui biar bisa undo creation
# def basic_cancel_ui(cancel_string,
#                     undo_string,
#                     undo_)


def siapakah_orgo():
    bubble_1 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1531760657/logo.jpg',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='ORGO line_bot_api', weight='bold', size='sm', align='center',
                              color=WARNA_ORANGE),
                SeparatorComponent(),
                TextComponent(
                    text='Orgo adalah bot cerdas yang memudahkan Anda mengoptimalkan jalannya grup/organisasi yang Anda ikuti.\n' + '\n' +
                         'Segala informasi dapat diakses melalui smartphone Anda', size='sm',
                    wrap=True),
            ]
        )
    )

    bubble_2 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1532191212/about-1.png',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='Ciamik & Mudah Digunakan', weight='bold', size='sm',
                              align='center', color=WARNA_ORANGE, wrap=True),
                SeparatorComponent(),
                TextComponent(
                    text='Orgo memiliki tampilan yang memukau dan mudah untuk digunakan karena dilengkapi dengan konten bantuan yang lengkap, Anda tidak akan mengalami kesulitan menggunakan Orgo',
                    size='sm', wrap=True),
            ]
        )
    )

    bubble_3 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1532183536/about-2.png',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='Mudah Akses Informasi', weight='bold', size='sm',
                              align='center', color=WARNA_ORANGE, wrap=True),
                SeparatorComponent(),
                TextComponent(
                    text='Dengan memanfaatkan Grup LINE, Orgo dapat membantu Anda mengelola & mengetahui segala informasi dari Grup/Organisasi yang Anda ikuti dimanapun dan kapanpun',
                    size='sm', wrap=True),
            ]
        )
    )

    bubble_4 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1532183536/about-2.png',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='Mudah Akses File', weight='bold', size='sm',
                              align='center', color=WARNA_ORANGE, wrap=True),
                SeparatorComponent(),
                TextComponent(
                    text='Orgo dapat menyimpan file & dokumen penting dari grup/organisasi, dan dapat diakses kapanpun dan dimanapun saat Anda membutuhkan. ' + \
                         'Data akan tersimpan dengan Aman',
                    size='sm', wrap=True),
            ]
        )
    )

    bubble_5 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1532183521/about-4.png',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='Kaya akan Fitur Keren!', weight='bold', size='sm',
                              align='center', color=WARNA_ORANGE, wrap=True),
                SeparatorComponent(),
                TextComponent(
                    text='Orgo menyediakan berbagai fitur lain yang tak kalah kerennya, seperti peramalan cuaca, terjemahan bahasa, pencarian gizi pada hidangan, dan lainnya\n' +
                         '\n' + 'Cobalah Orgo sekarang juga!',
                    size='sm', wrap=True),
            ]
        )
    )

    UI = CarouselContainer(
        contents=[bubble_1, bubble_2, bubble_3, bubble_4, bubble_5]
    )

    showUI = FlexSendMessage(alt_text="Tentang Admin Orgo", contents=UI)
    return showUI


def admin_orgo():
    bubble_1 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1531760657/logo.jpg',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                TextComponent(text='ORGO line_bot_api (Versi 1.0)', weight='bold', size='md',
                              color=WARNA_ORANGE),
                TextComponent(text='Solusi Manajemen Grup/Organisasi Anda', weight='bold',
                              size='sm', color='#aaaaaa', wrap=True),
                SeparatorComponent(),
                TextComponent(
                    text='Sebuah Mahakarya yang dirancang oleh Tim Maba Palsu Compfest X\n' + '\n' + 'Orgo Bot berbasis Python 3.6 , Menggunakan platform LINE & Pseudo-AI\n' + '\n',
                    size='sm', wrap=True),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_ORANGE,
                    action=PostbackAction(label='Kirim Umpan Balik', data='bantuan-feedback')),
                SeparatorComponent()
            ]
        )
    )

    bubble_2 = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                TextComponent(text='ADMIN ORGO', weight='bold', size='sm', align='center',
                              color=WARNA_ORANGE),
                SeparatorComponent(),
                TextComponent(text='Muhamad Abdurahman', weight='bold', size='md', color=WARNA_BIRU,
                              wrap=True),
                TextComponent(
                    text='20 Tahun\n' + 'Mahasiswa Ilmu Komputer UI - 2017\n' + 'Programmer Handal & Cerdas yang membuat Orgo menjadi cerdas & mampu memenuhi apa kebutuhan Anda',
                    size='sm', wrap=True),
                SeparatorComponent(),
                TextComponent(text='Rd Pradipta Gitaya S', weight='bold', size='md',
                              color=WARNA_BIRU, wrap=True),
                TextComponent(
                    text='21 Tahun\n' + 'Mahasiswa Sistem Informasi UI - 2017\n' + 'Programmer Handal & Perfeksionis yang membuat Orgo memiliki tampilan yang simpel, mudah dipahami & digunakan',
                    size='sm', wrap=True),
                SeparatorComponent(),
                TextComponent(text='Usman Sidiq', weight='bold', size='md', color=WARNA_BIRU,
                              wrap=True),
                TextComponent(
                    text='21 Tahun\n' + 'Mahasiswa Ilmu Komputer UI - 2017\n' + 'Designer Handal & Kreatif yang membuat Orgo tampil memukau & nyaman dihadapan Anda',
                    size='sm', wrap=True)
            ]
        )
    )

    UI = CarouselContainer(
        contents=[bubble_1, bubble_2]
    )

    showUI = FlexSendMessage(alt_text="Tentang Admin Orgo", contents=UI)
    return showUI


def konfirmasi_keluar(event):
    konfirmasiKeluar = ConfirmTemplate(text='Apakah Anda yakin ingin mengeluarkan Orgo dari sini?',
                                       actions=[
                                           PostbackAction(label='Ya', data='bot-keluar'),
                                           PostbackAction(label='Tidak', data='bot-tidak-keluar')])

    confirm = TemplateSendMessage(alt_text='Konfirmasi Keluarkan Orgo', template=konfirmasiKeluar)
    line_bot_api.reply_message(event.reply_token, confirm)


def bot_keluar(event):
    if isinstance(event.source, SourceGroup):
        line_bot_api.reply_message(event.reply_token, TextSendMessage(
            text='Terima kasih telah menggunakan Orgo! Orgo izin pamit dulu. Sampai Jumpa!'))
        line_bot_api.leave_group(event.source.group_id)
    elif isinstance(event.source, SourceRoom):
        line_bot_api.reply_message(event.reply_token, TextSendMessage(
            text='Terima kasih telah menggunakan Orgo! Orgo izin pamit dulu. Sampai Jumpa!'))
        line_bot_api.leave_room(event.source.room_id)
    else:
        line_bot_api.reply_message(event.reply_token, TextSendMessage(
            text="Mohon maaf, Orgo tidak bisa keluar dari personal chat/PM!"))


def group_column_ui(groups):

    group_column_list = []

    for group in groups:
        group_id = group.id
        bubble = BubbleContainer(
            direction='ltr',
            body=BoxComponent(
                layout='vertical', spacing='md',
                contents=[
                    TextComponent(group.name,
                                  weight='bold', align='center', size='md', color=WARNA_HITAM, wrap=True),
                    SeparatorComponent(),
                    TextComponent(text='Deskripsi:',
                                  weight='bold', size='xs', color='#aaaaaa', wrap=True),
                    TextComponent(text=group.descriptions, size='xs', wrap=True),
                    BoxComponent(
                        layout='horizontal', spacing='sm',
                        contents=[
                            TextComponent(text='Lihat Jadwal',
                                          color=WARNA_HITAM, size='sm', flex=4, weight='bold', gravity='center', wrap=True
                            ),
                            SeparatorComponent(),
                            ButtonComponent(
                                style='link',
                                height='sm',
                                color=WARNA_MERAH,
                                action=PostbackAction(label='>>',
                                                      data='show-group-schedules-' + group_id))
                        ],
                    ),
                    BoxComponent(
                        layout='horizontal',
                        spacing='sm',
                        contents=[
                            TextComponent(
                                text='Lihat Pengumuman',
                                color=WARNA_HITAM,
                                size='sm',
                                flex=4,
                                weight='bold',
                                gravity='center',
                                wrap=True
                            ),
                            SeparatorComponent(),
                            ButtonComponent(
                                style='link',
                                height='sm',
                                color=WARNA_MERAH,
                                action=PostbackAction(label='>>',
                                                      data='show-group-announcements-' + group_id))
                        ],
                    ),
                    BoxComponent(
                        layout='horizontal',
                        spacing='sm',
                        contents=[
                            TextComponent(
                                text='Program Kerja',
                                color=WARNA_HITAM,
                                size='sm',
                                flex=4,
                                weight='bold',
                                gravity='center',
                                wrap=True
                            ),
                            SeparatorComponent(),
                            ButtonComponent(
                                style='link',
                                height='sm',
                                color=WARNA_MERAH,
                                action=PostbackAction(label='>>',
                                                      data='show-group-workplans-' + group_id))
                        ],
                    ),
                    BoxComponent(
                        layout='horizontal',
                        spacing='sm',
                        contents=[
                            TextComponent(
                                text='Dokumen Tersimpan',
                                color=WARNA_HITAM,
                                size='sm',
                                flex=4,
                                weight='bold',
                                gravity='center',
                                wrap=True
                            ),
                            SeparatorComponent(),
                            ButtonComponent(
                                style='link',
                                height='sm',
                                color=WARNA_MERAH,
                                action=PostbackAction(label='>>',
                                                      data='show-group-documents-' + group_id))
                        ],
                    ),
                    BoxComponent(
                        layout='horizontal',
                        spacing='sm',
                        contents=[
                            TextComponent(
                                text='Pesan Anonim - Mode Rahasia',
                                color=WARNA_HITAM,
                                size='sm',
                                flex=4,
                                weight='bold',
                                gravity='center',
                                wrap=True
                            ),
                            SeparatorComponent(),
                            ButtonComponent(
                                style='link',
                                height='sm',
                                color=WARNA_MERAH,
                                action=PostbackAction(label='>>',
                                                      data='start-anonymous-' + group_id))
                        ],
                    ),
                    SeparatorComponent()
                ]
            )
        )
        group_column_list.append(bubble)

    if 1 <= len(group_column_list) <= 10:
        UI = CarouselContainer(contents=group_column_list)
        showUI = FlexSendMessage(alt_text="Daftar Grup/Organisasi", contents=UI)
        return showUI

    else:
        UI_1 = CarouselContainer(contents=group_column_list[:10])
        UI_2 = CarouselContainer(contents=group_column_list[10:])
        showUI_1 = FlexSendMessage(alt_text="Daftar Grup/Organisasi", contents=UI_1)
        showUI_2 = FlexSendMessage(alt_text="Daftar Grup/Organisasi", contents=UI_2)
        return [showUI_1, showUI_2]


def lihat_semua_organisasi(event, organizations):
    array_kolom = []

    for i in range(len(organizations)):
        organization_id = organizations[i].id
        bubble = BubbleContainer(
            direction='ltr',
            body=BoxComponent(
                layout='vertical',
                spacing='md',
                contents=[
                    TextComponent(organizations[i].name, weight='bold', align='center', size='md',
                                  color=WARNA_HITAM, wrap=True),
                    SeparatorComponent(),
                    TextComponent(text='Deskripsi:', weight='bold', size='xs', color='#aaaaaa',
                                  wrap=True),
                    TextComponent(text=organizations[i].descriptions, size='xs', wrap=True),
                    BoxComponent(
                        layout='horizontal',
                        spacing='sm',
                        contents=[
                            TextComponent(
                                text='Lihat Jadwal',
                                color=WARNA_HITAM,
                                size='sm',
                                flex=4,
                                weight='bold',
                                gravity='center',
                                wrap=True
                            ),
                            SeparatorComponent(),
                            ButtonComponent(
                                style='link',
                                height='sm',
                                color=WARNA_MERAH,
                                action=PostbackAction(label='>>',
                                                      data='lihat-jadwal-org-' + str(i)))
                        ],
                    ),
                    BoxComponent(
                        layout='horizontal',
                        spacing='sm',
                        contents=[
                            TextComponent(
                                text='Lihat Pengumuman',
                                color=WARNA_HITAM,
                                size='sm',
                                flex=4,
                                weight='bold',
                                gravity='center',
                                wrap=True
                            ),
                            SeparatorComponent(),
                            ButtonComponent(
                                style='link',
                                height='sm',
                                color=WARNA_MERAH,
                                action=PostbackAction(label='>>',
                                                      data='lihat-pengumuman-org-' + str(i)))
                        ],
                    ),
                    BoxComponent(
                        layout='horizontal',
                        spacing='sm',
                        contents=[
                            TextComponent(
                                text='Program Kerja',
                                color=WARNA_HITAM,
                                size='sm',
                                flex=4,
                                weight='bold',
                                gravity='center',
                                wrap=True
                            ),
                            SeparatorComponent(),
                            ButtonComponent(
                                style='link',
                                height='sm',
                                color=WARNA_MERAH,
                                action=PostbackAction(label='>>',
                                                      data='lihat-proker-org-' + str(i)))
                        ],
                    ),
                    BoxComponent(
                        layout='horizontal',
                        spacing='sm',
                        contents=[
                            TextComponent(
                                text='Dokumen Tersimpan',
                                color=WARNA_HITAM,
                                size='sm',
                                flex=4,
                                weight='bold',
                                gravity='center',
                                wrap=True
                            ),
                            SeparatorComponent(),
                            ButtonComponent(
                                style='link',
                                height='sm',
                                color=WARNA_MERAH,
                                action=PostbackAction(label='>>',
                                                      data='lihat-dokumen-org-' + str(i)))
                        ],
                    ),
                    BoxComponent(
                        layout='horizontal',
                        spacing='sm',
                        contents=[
                            TextComponent(
                                text='Pesan Anonim - Mode Rahasia',
                                color=WARNA_HITAM,
                                size='sm',
                                flex=4,
                                weight='bold',
                                gravity='center',
                                wrap=True
                            ),
                            SeparatorComponent(),
                            ButtonComponent(
                                style='link',
                                height='sm',
                                color=WARNA_MERAH,
                                action=PostbackAction(label='>>',
                                                      data='start-anonymous-' + organization_id))
                        ],
                    ),
                    SeparatorComponent()
                ]
            )
        )
        array_kolom.append(bubble)

    if 1 <= len(array_kolom) <= 10:
        UI = CarouselContainer(contents=array_kolom)
        showUI = FlexSendMessage(alt_text="Daftar Grup/Organisasi", contents=UI)
        return showUI

    else:
        UI_1 = CarouselContainer(contents=array_kolom[:10])
        UI_2 = CarouselContainer(contents=array_kolom[10:])
        showUI_1 = FlexSendMessage(alt_text="Daftar Grup/Organisasi", contents=UI_1)
        showUI_2 = FlexSendMessage(alt_text="Daftar Grup/Organisasi", contents=UI_2)
        return [showUI_1, showUI_2]


def get_menu_nongrup(event):
    bubble_1 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1531760657/menu-bg.jpg',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=[
                SeparatorComponent(),
                TextComponent(text='MENU UTAMA', weight='bold', size='md', color=WARNA_BIRU,
                              align='center'),
                SeparatorComponent(),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Lihat Jadwal',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='>>', text='/lihat_jadwal'))
                    ],
                ),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Lihat Pengumuman',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='>>', text='/lihat_pengumuman'))
                    ],
                ),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Lihat Proker',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='>>', text='/lihat_proker'))
                    ],
                ),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Lihat Dokumen',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='>>', text='/lihat_dokumen'))
                    ],
                ),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Grup/Organisasi Saya',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='>>', text='/lihat_semua'))
                    ],
                ),
                SeparatorComponent(),
                TextComponent(
                    text='Menu ini akan tampil berbeda saat Anda mengetik /menu pada grup chat',
                    weight='bold', align='center', color='#aaaaaa', size='xxs', wrap=True)
            ]
        )
    )

    bubble_2 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1531760657/menu-bg.jpg',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=[
                SeparatorComponent(),
                TextComponent(text='MENU UTAMA', weight='bold', size='md', color=WARNA_ORANGE,
                              align='center'),
                SeparatorComponent(),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Lihat / Sunting Profil',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_ORANGE,
                            action=MessageAction(label='>>', text='/lihat_profil'))
                    ],
                ),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Pusat Bantuan & Fitur Lain',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_ORANGE,
                            action=MessageAction(label='>>', text='/bantuan'))
                    ],
                ),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Reset Orgo',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_ORANGE,
                            action=MessageAction(label='>>', text='/reset'))
                    ],
                ),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Pengaturan (Segera Hadir)',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_ORANGE,
                            action=MessageAction(label='>>', text='/pengaturan'))
                    ],
                ),
            ]
        )
    )

    UI = CarouselContainer(
        contents=[bubble_1, bubble_2]
    )
    showUI = FlexSendMessage(alt_text="Menu Orgo", contents=UI)
    return showUI


def get_menu_grup(event):
    bubble_1 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1531760657/menu-bg.jpg',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='MENU UTAMA', weight='bold', size='sm', color=WARNA_HITAM,
                              align='center'),
                SeparatorComponent(),
                TextComponent(text='Daftarkan grup kepada Orgo Sekarang!', weight='bold', size='xs',
                              color='#aaaaaa', align='center', wrap=True),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_HITAM,
                    action=MessageAction(label='Tambah Org./Grup', text='/buat_grup')),
                SeparatorComponent(),
                TextComponent(text='Sudah mendaftarkan grup/organisasi ?', weight='bold', size='xs',
                              color='#aaaaaa', align='center', wrap=True),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_HITAM,
                    action=MessageAction(label='Daftar & Sinkron', text='/sync')),
                SeparatorComponent(),
                TextComponent(text='Ingin melihat grup/organisasi Anda ?', weight='bold', size='xs',
                              color='#aaaaaa', align='center', wrap=True),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_HITAM,
                    action=MessageAction(label='Lihat Org./Grup Saya', text='/lihat_semua')),
                SeparatorComponent(),
                TextComponent(text='Apakah Anda memerlukan bantuan ?', weight='bold', size='xs',
                              color='#aaaaaa', align='center', wrap=True),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_HITAM,
                    action=MessageAction(label='Pusat Bantuan', text='Bantuan')),
                SeparatorComponent(),
                TextComponent(
                    text='Menu ini akan tampil berbeda saat Anda mengetik /menu selain dalam grup chat\n' + '(Personal Chat/Multi Chat)',
                    weight='bold', align='center', size='xxs', wrap=True)
            ]
        )
    )

    bubble_2 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1531760657/menu-bg.jpg',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=[
                SeparatorComponent(),
                TextComponent(text='FITUR ORGO', weight='bold', size='sm', color=WARNA_BIRU,
                              align='center'),
                SeparatorComponent(),
                TextComponent(text='Mengelola Jadwal', weight='bold', size='xs',
                              align='center', wrap=True),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='Tambah', text='/buat_jadwal')),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='Lihat', text='/lihat_jadwal')),
                    ]
                ),
                SeparatorComponent(),
                TextComponent(text='Mengelola Pengumuman', weight='bold', size='xs',
                              align='center', wrap=True),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='Tambah', text='/buat_pengumuman')),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='Lihat', text='/lihat_pengumuman')),
                    ]
                ),
                SeparatorComponent(),
                TextComponent(text='Mengelola Program Kerja', weight='bold', size='xs',
                              align='center', wrap=True),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='Tambah', text='/buat_proker')),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='Lihat', text='/lihat_proker')),
                    ]
                ),
                SeparatorComponent(),
                TextComponent(text='Mengelola Dokumen', weight='bold', size='xs',
                              align='center', wrap=True),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='Tambah', text='/buat_dokumen')),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_BIRU,
                            action=MessageAction(label='Lihat', text='/lihat_dokumen')),
                    ]
                )
            ]
        )
    )

    bubble_3 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url='https://res.cloudinary.com/orgo-bot/image/upload/v1531760657/menu-bg.jpg',
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=[
                SeparatorComponent(),
                TextComponent(text='FITUR ORGO', weight='bold', size='sm', color=WARNA_MERAH,
                              align='center'),
                SeparatorComponent(),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Lihat/Ubah Profil Saya',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_MERAH,
                            action=MessageAction(label='>>', text='/lihat_profil'))
                            ],
                        ),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Kirim Umpan Balik',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_MERAH,
                            action=PostbackAction(label='>>', data='bantuan-feedback'))
                    ],
                ),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Reset Orgo',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_MERAH,
                            action=MessageAction(label='>>', text='/reset'))
                    ],
                ),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text='Keluarkan Orgo',
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center',
                            wrap=True
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_MERAH,
                            action=PostbackAction(label='>>', data='fitur-konfirmasi-keluar'))
                           ],
                        ),
                    ]
                )
            )
    UI = CarouselContainer(
        contents=[bubble_1, bubble_2, bubble_3]
    )
    showUI = FlexSendMessage(alt_text="Menu Orgo", contents=UI)
    return showUI


def pembuat_organisasi(organizations):
    UI = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='DETAIL GRUP/ORGANISASI', weight='bold', align='center',
                              size='sm', color=WARNA_HITAM, wrap=True),
                SeparatorComponent(),
                TextComponent(text='Nama:', weight='bold', size='xs', color='#aaaaaa', wrap=True),
                TextComponent(text=organizations[0], size='xs', wrap=True),
                TextComponent(text='Deskripsi:', weight='bold', size='xs', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=organizations[1], size='xs', wrap=True),
                SeparatorComponent(),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_HITAM,
                    action=PostbackAction(label='Daftarkan Organisasi', data='Ya, Lanjutkan!')),
                ButtonComponent(
                    style='secondary',
                    height='sm',
                    action=PostbackAction(label='Sunting Data Organisasi', data='Tidak')),
                SeparatorComponent()
            ]
        ),
    )
    return UI


def berhasil_membuat_organisasi(event):
    UI = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                TextComponent(text='GRUP/ORGANISASI TELAH TERDAFTAR', weight='bold', align='center',
                              size='xs', color=WARNA_HITAM, wrap=True),
                SeparatorComponent(),
                TextComponent(
                    text='Kepada semua anggota grup/organisasi ini, silakan menekan tombol di bawah ini untuk menjadi anggota & sinkron semua informasi grup ini ke profil Anda\n' + '\n' +
                         'Anda juga bisa melakukan aksi ini dengan mengetik /sync', weight='bold',
                    size='xs', color='#aaaaaa', wrap=True),
                SeparatorComponent(),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_HITAM,
                    action=MessageAction(label='Daftar & Sinkron', text='/sync')),
                SeparatorComponent()
            ]
        ),
    )
    showUI = FlexSendMessage(alt_text="Daftar & Sinkron, Ketik: /sync", contents=UI)
    line_bot_api.reply_message(event.reply_token, showUI)


def pembuat_jadwal(event, schedules):
    UI = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='DETAIL JADWAL', weight='bold', align='center', size='sm',
                              color=WARNA_MERAH, wrap=True),
                SeparatorComponent(),
                TextComponent(text='Nama Jadwal:', weight='bold', size='xs', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=schedules[0], size='xs', wrap=True),
                TextComponent(text='Lokasi Pelaksanaan:', weight='bold', size='xs', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=schedules[1], size='xs', wrap=True),
                TextComponent(text='Waktu Pelaksanaan:', weight='bold', size='xs', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=schedules[2], size='xs', wrap=True),
                TextComponent(text='Deskripsi Jadwal:', weight='bold', size='xs', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=schedules[3], size='xs', wrap=True),
                SeparatorComponent(),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        ButtonComponent(
                            style='primary',
                            height='sm',
                            color=WARNA_MERAH,
                            action=MessageAction(label='Tambah', text='Ya, Lanjutkan!')),
                        ButtonComponent(
                            style='secondary',
                            height='sm',
                            action=MessageAction(label='Sunting', text='Tidak'))
                    ]
                ),
                SeparatorComponent()
            ]
        ),
    )
    showUI = FlexSendMessage(alt_text="Konfirmasi Jadwal", contents=UI)
    line_bot_api.reply_message(event.reply_token, showUI)


def get_organization_schedule_ui(event, schedules, organization_name):
    array_kolom = []

    if len(schedules) == 0:
        line_bot_api.reply_message(event.reply_token, TextSendMessage(
            "Mohon maaf, Grup/Organisasi ini belum memiliki jadwal apapun yang terdaftar"))

    elif len(schedules) > 20:
        line_bot_api.reply_message(event.reply_token,
                          TextSendMessage("Mohon maaf, jumlah jadwal melebihi batas (20)"))

    else:
        for i in range(len(schedules)):
            bubble = BubbleContainer(
                direction='ltr',
                body=BoxComponent(
                    layout='vertical',
                    spacing='md',
                    contents=[
                        TextComponent(text="DAFTAR JADWAL", weight='bold', align='center',
                                      size='lg', color=WARNA_HITAM, wrap=True),
                        SeparatorComponent(),
                        TextComponent(text=schedules[i].name, weight='bold', align='center',
                                      size='sm', color=WARNA_MERAH, wrap=True),
                        SeparatorComponent(),
                        TextComponent(text='Lokasi Pelaksanaan:', weight='bold', size='xs',
                                      color='#aaaaaa', wrap=True),
                        TextComponent(text=schedules[i].location, size='xs', wrap=True),
                        TextComponent(text='Waktu Pelaksanaan:', weight='bold', size='xs',
                                      color='#aaaaaa', wrap=True),
                        TextComponent(text=beautify_db_datetime_format(schedules[i].date),
                                      size='xs', wrap=True),
                        TextComponent(text='Deskripsi Jadwal:', weight='bold', size='xs',
                                      color='#aaaaaa', wrap=True),
                        TextComponent(text=schedules[i].descriptions, size='xs', wrap=True)
                    ]
                )
            )
            array_kolom.append(bubble)
            # print("testa")

        if 1 <= len(array_kolom) <= 10:
            UI = CarouselContainer(contents=array_kolom)
            showUI = FlexSendMessage(alt_text="Daftar Jadwal", contents=UI)
            return showUI

        else:
            UI_1 = CarouselContainer(contents=array_kolom[:10])
            UI_2 = CarouselContainer(contents=array_kolom[10:])
            showUI_1 = FlexSendMessage(alt_text="Daftar Jadwal", contents=UI_1)
            showUI_2 = FlexSendMessage(alt_text="Daftar Jadwal", contents=UI_2)
            return [showUI_1, showUI_2]


def pembuat_pengumuman(event, announcements):
    UI = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='DETAIL PENGUMUMAN', weight='bold', align='center', size='sm',
                              color=WARNA_MERAH, wrap=True),
                SeparatorComponent(),
                TextComponent(text='Judul Pengumuman:', weight='bold', size='xs', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=announcements[0], size='xs', wrap=True),
                TextComponent(text='Isi:', weight='bold', size='xs', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=announcements[1], size='xs', color='#aaaaaa', wrap=True),
                SeparatorComponent(),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        ButtonComponent(
                            style='primary',
                            height='sm',
                            color=WARNA_MERAH,
                            action=MessageAction(label='Tambah', text='Ya, Lanjutkan!')),
                        ButtonComponent(
                            style='secondary',
                            height='sm',
                            action=MessageAction(label='Sunting', text='Tidak'))
                    ]
                ),
                SeparatorComponent()
            ]
        ),
    )
    showUI = FlexSendMessage(alt_text="Konfirmasi Jadwal", contents=UI)
    line_bot_api.reply_message(event.reply_token, showUI)


def lihat_pengumuman_organisasi(announcements, show_edit=True):
    array_kolom = []

    if announcements is None:
        announcements = []

    if len(announcements) == 0:
        reply("Mohon maaf, Grup/Organisasi ini belum memiliki pengumuman apapun yang terdaftar")

    elif len(announcements) > 20:
        reply("Mohon maaf, jumlah pengumuman melebihi batas (20)")

    else:
        for i in range(len(announcements)):
            bottom = []
            if show_edit:
                bottom = [
                        ButtonComponent(
                            style='primary',
                            height='sm',
                            color=WARNA_HITAM,
                            action=PostbackAction(label='Sunting Pengumuman',
                                                  data='edit-announcement-' + str(
                                                      announcements[i].id))),
                        ButtonComponent(
                            style='primary',
                            height='sm',
                            color=WARNA_MERAH,
                            action=PostbackAction(label='Hapus Pengumuman',
                                                  data='delete-announcement-' + str(
                                                      announcements[i].id))),]
            contents = [
                        TextComponent(text="DAFTAR PENGUMUMAN", weight='bold', align='center',
                                      size='lg', color=WARNA_HITAM, wrap=True),
                        SeparatorComponent(),
                        TextComponent(text=announcements[i].title, weight='bold', align='center',
                                      size='sm', color=WARNA_MERAH, wrap=True),
                        SeparatorComponent(),
                        TextComponent(text='Deskripsi Pengumuman:', weight='bold', size='xs',
                                      color='#aaaaaa', wrap=True),
                        TextComponent(text=announcements[i].text, size='xs', wrap=True)
                    ]
            contents.extend(bottom)


            bubble = BubbleContainer(
                direction='ltr',
                body=BoxComponent(
                    layout='vertical',
                    spacing='md',
                    contents=contents
                ),
            )
            array_kolom.append(bubble)

        if 1 <= len(array_kolom) <= 10:
            UI = CarouselContainer(contents=array_kolom)
            showUI = FlexSendMessage(alt_text="Daftar Pengumuman", contents=UI)
            reply(showUI)

        else:
            UI_1 = CarouselContainer(contents=array_kolom[:10])
            UI_2 = CarouselContainer(contents=array_kolom[10:])
            showUI_1 = FlexSendMessage(alt_text="Daftar Pengumuman", contents=UI_1)
            showUI_2 = FlexSendMessage(alt_text="Daftar Pengumuman", contents=UI_2)
            reply([showUI_1, showUI_2])


def pembuat_proker(event, workplans):
    UI = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='DETAIL PROKER', weight='bold', align='center', size='sm',
                              color=WARNA_MERAH, wrap=True),
                SeparatorComponent(),
                TextComponent(text='Nama Proker:', weight='bold', size='xs', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=workplans[0], size='xs', wrap=True),
                TextComponent(text='Penanggung Jawab:', weight='bold', size='xs', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=workplans[1], size='xs', wrap=True),
                TextComponent(text='Deskripsi Proker:', weight='bold', size='xs', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=workplans[2], size='xs', wrap=True),
                SeparatorComponent(),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        ButtonComponent(
                            style='primary',
                            height='sm',
                            color=WARNA_MERAH,
                            action=MessageAction(label='Tambah', text='Ya, Lanjutkan!')),
                        ButtonComponent(
                            style='secondary',
                            height='sm',
                            action=MessageAction(label='Sunting', text='Tidak'))
                    ]
                ),
                SeparatorComponent()
            ]
        ),
    )
    showUI = FlexSendMessage(alt_text="Konfirmasi Proker", contents=UI)
    line_bot_api.reply_message(event.reply_token, showUI)


def brief_workplan_ui(workplans):
    # done_icon_url = 'https://res.cloudinary.com/orgo-bot/image/upload/v1532183757/if_Tick_Mark_Dark_1398912.png'
    # notdone_icon_url = 'https://res.cloudinary.com/orgo-bot/image/upload/v1532183757/ico_putih.png'
    progres = str(get_workplans_progress(workplans))
    contents = [
        TextComponent(text="PROGRES : " + progres + "%", weight='bold', align='center',
                      size='lg', color=WARNA_HIJAU, wrap=True),
        SeparatorComponent()
    ]
    for workplan in workplans:
        if workplan.is_done == "true":
            font_color = WARNA_HIJAU
            # icon_url = done_icon_url
        else:
            font_color = WARNA_HITAM
            # icon_url = notdone_icon_url
        # print("wo")
        box = BoxComponent(
            layout='horizontal',
            margin='sm',
            contents=[
                TextComponent(
                    text=workplan.name,
                    color=font_color,
                    size='sm',
                    flex=4,
                    weight='bold',
                    gravity='center',
                    wrap=True
                ),
                SeparatorComponent(),
                ButtonComponent(
                    style='link',
                    height='sm',
                    color=WARNA_MERAH,
                    action=PostbackAction(data="show-workplan-" + str(workplan.id), label='>>')
                )
            ]
        )
        contents.append(box)

    UI = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            # contents=contents
            contents=contents
        )
    )
    showUI = FlexSendMessage(alt_text="Tampilkan Program Kerja", contents=UI)
    return showUI


def brief_schedules_ui(schedules, title: str):
    contents = [
        TextComponent(text=title, weight='bold', align='center',
                      size='lg', color=WARNA_HITAM, wrap=True),
        SeparatorComponent()
    ]
    for schedule in schedules:
        box = BoxComponent(
            layout='horizontal',
            flex=4,
            margin='sm',
            contents=[
                BoxComponent(
                    layout='vertical',
                    margin='sm',
                    flex=4,
                    contents=[
                        TextComponent(
                            text=beautify_db_datetime_format(schedule.date),
                            color=WARNA_MERAH,
                            size='sm',
                            flex=4,
                            wrap=True,
                            weight='bold',
                            gravity='center'
                        ),
                        TextComponent(
                            text=schedule.name,
                            color=WARNA_HITAM,
                            size='sm',
                            weight='bold',
                            wrap=True,
                            gravity='center'
                        )
                    ]
                ),
                SeparatorComponent(),
                ButtonComponent(
                    style='link',
                    height='sm',
                    color=WARNA_MERAH,
                    action=PostbackAction(data="show-schedule-" + str(schedule.id), label='>>')
                )
            ]
        )

        contents.append(box)
        contents.append(SeparatorComponent())

    UI = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=contents
        )
    )
    showUI = FlexSendMessage(alt_text="Jadwal Pengguna", contents=UI)
    return showUI


def lihat_proker_organisasi(event, workplans, organization_name):
    event = get_current_event()
    array_kolom = []

    if len(workplans) == 0:
        line_bot_api.reply_message(event.reply_token, TextSendMessage(
            "Mohon maaf, Grup/Organisasi ini belum memiliki Program Kerja apapun yang terdaftar"))

    elif len(workplans) > 20:
        line_bot_api.reply_message(event.reply_token, TextSendMessage(
            "Mohon maaf, jumlah Program Kerja melebihi batas (20)"))

    else:
        for i in range(len(workplans)):
            workplan_id = str(workplans[i].id)
            # print("i")
            # print(i)
            is_done = workplans[i].is_done
            if is_done == "true":
                is_done_color = WARNA_HITAM
                button_color = WARNA_HITAM
                button_text = "Buat jadi belum tuntas"
                postback_data = "notdone-workplan-"
                icon_url = 'https://i.imgur.com/3Wf7wen.png'
                icon = IconComponent(size='xs', url=icon_url)
            else:
                is_done_color = WARNA_HITAM
                button_color = WARNA_HIJAU
                button_text = "Buat jadi tuntas"
                postback_data = "done-workplan-"
                icon_url = 'https://res.cloudinary.com/orgo-bot/image/upload/v1532183757/ico_putih.png'
                icon = TextComponent(text=' ', size='xs', color=is_done_color,margin='md', flex=0)
            is_done = workplan_bool_to_string(is_done)
            # print(type(is_done))
            # print(is_done)
            bubble = BubbleContainer(
                direction='ltr',
                body=BoxComponent(
                    layout='vertical',
                    spacing='md',
                    contents=[
                        TextComponent("PROGRAM KERJA", weight='bold', align='center', size='lg',
                                      color=WARNA_HITAM, wrap=True),
                        SeparatorComponent(),
                        TextComponent(text=workplans[i].name, weight='bold', align='center',
                                      size='sm', color=WARNA_MERAH, wrap=True),
                        SeparatorComponent(),
                        TextComponent(text='Status :', weight='bold', size='xs',
                                      color='#aaaaaa', wrap=True),
                        BoxComponent(
                            layout='baseline',
                            margin='md',
                            contents=[
                                icon,
                                TextComponent(text=is_done, size='xs', color=is_done_color,
                                              margin='md', flex=0)
                            ]
                        ),
                        TextComponent(text='Penanggung Jawab:', weight='bold', size='xs',
                                      color='#aaaaaa', wrap=True),
                        TextComponent(text=workplans[i].person_in_charge, size='xs', wrap=True),
                        TextComponent(text='Deskripsi Program Kerja:', weight='bold', size='xs',
                                      color='#aaaaaa', wrap=True),
                        TextComponent(text=workplans[i].descriptions, size='xs', wrap=True),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='primary',
                            height='sm',
                            color=button_color,
                            action=PostbackAction(label=button_text,
                                                  data=postback_data + workplan_id)),
                        ButtonComponent(
                            style='primary',
                            height='sm',
                            color=WARNA_MERAH,
                            action=PostbackAction(label="Hapus Program Kerja",
                                                  data="delete-workplan-" + workplan_id))
                    ]
                )
            )
            array_kolom.append(bubble)

        if 1 <= len(array_kolom) <= 10:
            UI = CarouselContainer(contents=array_kolom)
            showUI = FlexSendMessage(alt_text="Daftar Proker", contents=UI)
            line_bot_api.reply_message(event.reply_token, showUI)

        else:
            UI_1 = CarouselContainer(contents=array_kolom[:10])
            UI_2 = CarouselContainer(contents=array_kolom[10:])
            showUI_1 = FlexSendMessage(alt_text="Daftar Proker", contents=UI_1)
            showUI_2 = FlexSendMessage(alt_text="Daftar Proker", contents=UI_2)
            line_bot_api.reply_message(event.reply_token, [showUI_1, showUI_2])


def get_workplan_ui(workplan: Workplan, editable=True):
    event = get_current_event()
    is_done = workplan.is_done
    if is_done == "true":
        is_done_color = "#999999"
        button_color = WARNA_HITAM
        button_text = "Buat jadi belum tuntas"
        postback_data = "notdone-workplan-"
        icon_url = 'https://res.cloudinary.com/orgo-bot/image/upload/v1532183757/if_Tick_Mark_Dark_1398912.png'
    else:
        is_done_color = WARNA_HITAM
        button_color = "#41AD4A"
        button_text = "Buat jadi tuntas"
        postback_data = "done-workplan-"
        icon_url = 'https://res.cloudinary.com/orgo-bot/image/upload/v1532183757/ico_putih.png'
    is_done = workplan_bool_to_string(is_done)
    if editable:
        edit_ui = [
            ButtonComponent(
                style='primary',
                height='sm',
                color=button_color,
                action=PostbackAction(label=button_text,
                                      data=postback_data + workplan.id)),
            ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_HITAM,
                action=PostbackAction(label="Sunting Proker",
                                      data="edit-workplan-" + workplan.id)),
            ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_MERAH,
                action=PostbackAction(label="Hapus Proker",
                                      data="delete-workplan-" + workplan.id))

        ]
    else:
        edit_ui = []
    contents = [
        TextComponent("PROGRAM KERJA", weight='bold', align='center', size='lg',
                      color=WARNA_HITAM, wrap=True),
        SeparatorComponent(),
        TextComponent(text=workplan.name, weight='bold', align='center',
                      size='sm', color=WARNA_MERAH, wrap=True),
        SeparatorComponent(),
        TextComponent(text='Status :', weight='bold', size='xs',
                      color='#aaaaaa', wrap=True),
        BoxComponent(
            layout='baseline',
            margin='md',
            contents=[
                IconComponent(size='xs', url=icon_url),
                TextComponent(text=is_done, size='xs', color=is_done_color,
                              margin='md', flex=0)
            ]
        ),
        TextComponent(text='Penanggung Jawab:', weight='bold', size='xs',
                      color='#aaaaaa', wrap=True),
        TextComponent(text=workplan.person_in_charge, size='xs', wrap=True),
        TextComponent(text='Deskripsi Program Kerja:', weight='bold', size='xs',
                      color='#aaaaaa', wrap=True),
        TextComponent(text=workplan.descriptions, size='xs', wrap=True),
        SeparatorComponent(),
    ]
    contents.extend(edit_ui)
    bubble = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=contents
        )
    )

    showUI = FlexSendMessage(alt_text="Rincian Program Kerja", contents=bubble)
    return showUI


def get_schedule_ui(schedule: Schedule, editable=True):
    if editable:
        edit_ui = [
            ButtonComponent(
            style='primary',
            height='sm',
            color=WARNA_HITAM,
            action=PostbackAction(label="Sunting Jadwal",
                                  data="edit-schedule-" + schedule.id)),
        ButtonComponent(
            style='primary',
            height='sm',
            color=WARNA_MERAH,
            action=PostbackAction(label="Hapus Jadwal",
                                  data="delete-schedule-" + schedule.id))
        ]
    else:
        edit_ui = []

    contents=[
        TextComponent(text="RINCIAN JADWAL", weight='bold', align='center',
                      size='lg', color=WARNA_HITAM, wrap=True),
        SeparatorComponent(),
        TextComponent(text=schedule.name, weight='bold', align='center',
                      size='sm', color=WARNA_MERAH, wrap=True),
        SeparatorComponent(),
        TextComponent(text='Lokasi Pelaksanaan:', weight='bold', size='xs',
                      color='#aaaaaa', wrap=True),
        TextComponent(text=schedule.location, size='xs', wrap=True),
        TextComponent(text='Waktu Pelaksanaan:', weight='bold', size='xs',
                      color='#aaaaaa', wrap=True),
        TextComponent(text=beautify_db_datetime_format(schedule.date),
                      size='xs', wrap=True),
        TextComponent(text='Deskripsi Jadwal:', weight='bold', size='xs',
                      color='#aaaaaa', wrap=True),
        TextComponent(text=schedule.descriptions, size='xs', wrap=True),
        SeparatorComponent(),
   ]
    contents.extend(edit_ui)
    bubble = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=contents
        )
    )

    showUI = FlexSendMessage(alt_text="Daftar Proker", contents=bubble)
    return showUI


def get_edit_schedule_ui(schedule: Schedule):
    bubble = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=[
                TextComponent(text="Rincian Jadwal", weight='bold', align='center',
                              size='lg', color=WARNA_HITAM, wrap=True),
                SeparatorComponent(),
                TextComponent(text=schedule.name, size='xs', wrap=True),
                ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_HITAM,
                action=PostbackAction(label="Sunting Judul",
                                      data="edit-schedule-title-" + schedule.id)),

                TextComponent(text=schedule.location, size='xs', wrap=True),
                ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_HITAM,
                action=PostbackAction(label="Sunting Lokasi",
                                      data="edit-schedule-location-" + schedule.id)),

                TextComponent(text=beautify_db_datetime_format(schedule.date),
                              size='xs', wrap=True),
                ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_HITAM,
                action=PostbackAction(label="Sunting Waktu",
                                      data="edit-schedule-date-" + schedule.id)),

                TextComponent(text=schedule.descriptions, size='xs', wrap=True),
                ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_HITAM,
                action=PostbackAction(label="Sunting Deskripsi",
                                      data="edit-schedule-descriptions-" + schedule.id)),
            ]
        )
    )

    showUI = FlexSendMessage(alt_text="Rincian Jadwal", contents=bubble)
    return showUI


def get_edit_workplan_ui(workplan: Workplan):
    bubble = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=[
                TextComponent(text="Rincian Program Kerja", weight='bold', align='center',
                              size='lg', color=WARNA_HITAM, wrap=True),
                SeparatorComponent(),

                TextComponent(text=workplan.name, size='xs', wrap=True),
                ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_HITAM,
                action=PostbackAction(label="Sunting Nama",
                                      data="edit-workplan-name-" + workplan.id)),

                TextComponent(text=workplan.person_in_charge, size='xs', wrap=True),
                ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_HITAM,
                action=PostbackAction(label="Sunting Penanggung Jawab",
                                      data="edit-workplan-pic-" + workplan.id)),

                TextComponent(text=workplan.descriptions, size='xs', wrap=True),
                ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_HITAM,
                action=PostbackAction(label="Sunting Deskripsi",
                                      data="edit-workplan-descriptions-" + workplan.id)),
            ]
        )
    )

    showUI = FlexSendMessage(alt_text="Rincian Jadwal", contents=bubble)
    return showUI


def get_edit_announcement_ui(announcement: Announcement):
    bubble = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=[
                TextComponent(text="Rincian Pengumuman", weight='bold', align='center',
                              size='lg', color=WARNA_HITAM, wrap=True),
                SeparatorComponent(),
                TextComponent(text=announcement.title, size='xs', wrap=True),
                ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_HITAM,
                action=PostbackAction(label="Sunting Judul",
                                      data="edit-announcement-title-" + announcement.id)),

                TextComponent(text=announcement.text, size='xs', wrap=True),
                ButtonComponent(
                style='primary',
                height='sm',
                color=WARNA_HITAM,
                action=PostbackAction(label="Sunting Isi",
                                      data="edit-announcement-text-" + announcement.id)),
            ]
        )
    )

    showUI = FlexSendMessage(alt_text="Rincian Pengumuman", contents=bubble)
    return showUI


def lihat_dokumen_organisasi(event, files, organization_name):
    if len(files) == 0:
        line_bot_api.reply_message(event.reply_token, TextSendMessage(
            text='Mohon maaf, Grup/Organisasi ini belum memiliki dokumen yang tersimpan di Orgo'))

    else:
        daftar_konten = [
            TextComponent(text='DAFTAR DOKUMEN', weight='bold', align='center', size='lg',
                          color=WARNA_HITAM, wrap=True),
            TextComponent(text=organization_name, weight='bold', align='center', size='md',
                          color=WARNA_MERAH, wrap=True),
            SeparatorComponent(),
            TextComponent(text="Tekan >> Untuk Mengunduh", size='sm', align='center', weight='bold',
                          color='#aaaaaa', wrap=True)
        ]

        for i in files:
            link_file = str(i.url)
            daftar_konten.append(
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        TextComponent(
                            text=i.name,
                            color=WARNA_HITAM,
                            size='sm',
                            flex=4,
                            weight='bold',
                            gravity='center'
                        ),
                        SeparatorComponent(),
                        ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_MERAH,
                            action=MessageAction(text="Link Unduh:\n" + link_file, label='>>'))
                    ],
                )
            )

        UI = BubbleContainer(
            direction='ltr',
            body=BoxComponent(
                layout='vertical',
                spacing='sm',
                contents=daftar_konten
            ),
        )
        showUI = FlexSendMessage(alt_text="Daftar Dokumen Tersimpan", contents=UI)
        line_bot_api.reply_message(event.reply_token, showUI)


def profil_saya_kosong(event):
    pengirim = line_bot_api.get_profile(event.source.user_id)

    status = pengirim.status_message
    if status == None:
        status = "Tidak ada pesan status"

    if pengirim.picture_url == None:
        image_link = 'https://res.cloudinary.com/orgo-bot/image/upload/v1531760565/Man-no-image.jpg'

    else:
        image_link = upload_image(pengirim.picture_url, event.source.user_id)

    bubble_1 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url=image_link,
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='lg',
            contents=[
                TextComponent(text=pengirim.display_name, weight='bold', color=WARNA_MERAH,
                              size='md', wrap=True),
                TextComponent(text=status, size='sm', wrap=True),
                SeparatorComponent(),
                TextComponent(text='Perhatian!', weight='bold', color=WARNA_MERAH, align='center',
                              size='xs', wrap=True),
                TextComponent(
                    text='Belum ada data diri. Silakan tekan tombol di bawah untuk menambahkan identitas Anda',
                    weight='bold', align='center', size='xs', wrap=True),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_MERAH,
                    action=MessageAction(label='Isi Identitas', text='/buat_profil')),
            ]
        )
    )
    showUI = FlexSendMessage(alt_text="Profil Saya", contents=bubble_1)
    return showUI


def profil_saya_lengkap(event, profile, position):
    pengirim = line_bot_api.get_profile(event.source.user_id)
    status = pengirim.status_message


    if status == None:
        status = "Tidak ada pesan status"
    if pengirim.picture_url == None:
        image_link = 'https://res.cloudinary.com/orgo-bot/image/upload/v1531760565/Man-no-image.jpg'
    else:
        image_link = upload_image(pengirim.picture_url, event.source.user_id)
    if profile.bod == ' ':
        bod = 'Belum ditambahkan'
    else:
        bod = profile.bod
    if profile.skill == ' ':
        skill = 'Belum ditambahkan'
    else:
        skill = profile.skill

    bubble_1 = BubbleContainer(
        direction='ltr',
        hero=ImageComponent(
            url=image_link,
            size='full',
            aspect_ratio='20:13',
            aspect_mode='cover',
        ),
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=[
                TextComponent(text=profile.name, weight='bold', color=WARNA_MERAH, size='md',
                              wrap=True),
                TextComponent(text=status, size='xs', wrap=True),
                SeparatorComponent(),
                TextComponent(text="Tanggal Lahir:", size='sm', weight='bold',
                              color='#aaaaaa', wrap=True),
                TextComponent(text=bod, size='sm', wrap=True),
                TextComponent(text="Keahlian saya:", size='sm', weight='bold', color='#aaaaaa',
                              wrap=True),
                TextComponent(text=skill, size='sm', wrap=True),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_MERAH,
                    action=MessageAction(label='Sunting Profil Saya', text='/buat_profil')),
            ]
        )
    )
    inner_content = [
        TextComponent(text="KEANGGOTAAN SAYA", weight='bold', color=WARNA_MERAH, size='md',
                      align='center', wrap=True),
        TextComponent(text="Grup/Organisasi Anda - Jabatan", size='sm', weight='bold',
                      color='#aaaaaa', align='center', wrap=True),
        SeparatorComponent(),
    ]

    for i in range(len(position)):
        org = position[i][0]
        jabatan = position[i][1]

        if jabatan == 'Eror tidak ada':
            jabatan = "Jabatan belum ditambahkan"

        inner_content.append(
            TextComponent(text=org, size='sm', weight='bold', color=WARNA_MERAH, wrap=True),
        )
        inner_content.append(
            TextComponent(text=jabatan, size='sm', wrap=True),
        )

    bubble_2 = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=inner_content
        )
    )

    UI = CarouselContainer(
        contents=[bubble_1, bubble_2]
    )

    showUI = FlexSendMessage(alt_text="Profil Saya", contents=UI)
    return showUI

def pembuat_profil_nongrup(event, profile_data):
    UI = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                SeparatorComponent(),
                TextComponent(text='DETAIL DATA DIRI', weight='bold', align='center', size='sm',
                              color=WARNA_MERAH, wrap=True),
                SeparatorComponent(),
                TextComponent(text='Nama:', weight='bold', size='xs', color='#aaaaaa', wrap=True),
                TextComponent(text=profile_data[1], size='xs', wrap=True),
                TextComponent(text='Tempat & Tanggal Lahir:', weight='bold', size='xs',
                              color='#aaaaaa', wrap=True),
                TextComponent(text=profile_data[2], size='xs', wrap=True),
                TextComponent(text='Jabatan di Grup/Organisasi ini:', weight='bold', size='xs',
                              color='#aaaaaa', wrap=True),
                TextComponent(text=profile_data[3], size='xs', wrap=True),
                TextComponent(text='Skill:', weight='bold', size='xs', color='#aaaaaa', wrap=True),
                TextComponent(text=profile_data[4], size='xs', wrap=True),

                SeparatorComponent(),
                BoxComponent(
                    layout='horizontal',
                    spacing='sm',
                    contents=[
                        ButtonComponent(
                            style='primary',
                            height='sm',
                            color=WARNA_MERAH,
                            action=MessageAction(label='Tambah', text='Ya, Lanjutkan!')),
                        ButtonComponent(
                            style='secondary',
                            height='sm',
                            action=MessageAction(label='Sunting', text='Tidak'))
                    ]
                ),
                SeparatorComponent()
            ]
        ),
    )
    showUI = FlexSendMessage(alt_text="Konfirmasi Biodata", contents=UI)
    line_bot_api.reply_message(event.reply_token, showUI)


def memilih_org(event, mode, organizations):
    daftar_konten = [
        TextComponent(text='DAFTAR GRUP/ORG. ANDA', weight='bold', size='md', color=WARNA_MERAH,
                      align='center'),
        TextComponent(text='Silakan pilih grup yang akan diproses', weight='bold', size='sm',
                      color='#aaaaaa', align='center', wrap=True),
        SeparatorComponent()
    ]
    for i in range(len(organizations)):
        daftar_konten.append(
            BoxComponent(
                layout='horizontal',
                spacing='sm',
                contents=[
                    TextComponent(
                        text=organizations[i].name,
                        color=WARNA_HITAM,
                        size='sm',
                        flex=4,
                        weight='bold',
                        gravity='center',
                        wrap=True
                    ),
                    SeparatorComponent(),
                    ButtonComponent(
                        style='link',
                        height='sm',
                        color=WARNA_MERAH,
                        action=PostbackAction(label='>>', data='lihat-' + mode + '-org-' + str(i)))
                ],
            )
        )

    bubble_1 = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=daftar_konten
        )
    )

    showUI = FlexSendMessage(alt_text="Memilih Grup/Organisasi", contents=bubble_1)
    return showUI


def selamat_datang_grup(event):
    UI = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='md',
            contents=[
                TextComponent(text='TERIMA KASIH', weight='bold', align='center', size='md',
                              color=WARNA_BIRU, wrap=True),
                TextComponent(text='Telah Menambahkan Orgo', weight='bold', size='xs',
                              align='center', color='#aaaaaa', wrap=True),
                SeparatorComponent(),
                TextComponent(text='Orgo adalah chatbot yang akan membantu Anda dalam mengatur '
                                   'grup/organisasi ini.\n'
                                   + '\n'
                                   + 'Untuk dapat menggunakan fitur Orgo secara penuh, silakan '
                                     'daftarkan terlebih dahulu grup/organisasi ini ' + \
                                   'dengan menekan tombol berwarna biru di bawah ini',
                              size='sm',
                              wrap=True),
                SeparatorComponent(),
                ButtonComponent(
                    style='primary',
                    height='sm',
                    color=WARNA_BIRU,
                    action=MessageAction(label='Daftarkan Sekarang',
                                         text='/buat_grup')
                ),
                ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_ORANGE,
                            action=MessageAction(label='Butuh Bantuan ?',
                                                 text='/bantuan')
                ),
                ButtonComponent(
                            style='link',
                            height='sm',
                            color=WARNA_ORANGE,
                            action=MessageAction(label='Menu Utama',
                                                 text='/menu')
                )
            ]
        )
    )

    msg =   (
            "Anda dapat pula melakukan dengan kata kunci:\n" + "\n" +
            "/buat_grup untuk mendaftarkan grup ke Orgo\n" +
            "/bantuan untuk menuju Pusat Bantuan & Fitur Lain\n" +
            "/menu untuk menuju Menu Utama\n" + '\n' +
            "(Jangan lupa tanda / nya)"
             )

    smart_city_msg = (
'''
===========================
Orgo & Smart City
===========================
Orgo adalah chatbot yang mengusung tema gabungan antara Smart People & Smart Mobility
    
Nama Orgo diambil dari “ORGanisasi yang terOrganisir”, dibuat \
untuk memberikan kemudahan mengelola dan mengorganisir informasi penting \
dalam grup seperti pengumuman, jadwal, program kerja, file dan dokumen \
agar lebih teratur kapanpun dan dimanapun tanpa dibatasi jarak dan waktu.
    
Dengan Orgo, setiap anggota grup tidak akan lagi terlewat melihat \
pengumuman, lupa akan jadwal, maupun kesulitan dalam mencari \
file dan dokumen penting dalam organisasi.
'''
    )

    showUI = FlexSendMessage(alt_text="Terima Kasih Telah Mengundang Orgo", contents=UI)
    line_bot_api.reply_message(event.reply_token, [showUI, TextSendMessage(text=msg), TextSendMessage(text=smart_city_msg) ] )


def selamat_datang_nongrup(event):
    UI = BubbleContainer(
        direction='ltr',
        body=BoxComponent(
            layout='vertical',
            spacing='sm',
            contents=[
                TextComponent(text='TERIMA KASIH', weight='bold', align='center', size='md',
                              color=WARNA_BIRU, wrap=True),
                TextComponent(text='Telah Menambahkan Orgo', weight='bold', size='xs',
                              align='center', color='#aaaaaa', wrap=True),
                SeparatorComponent(),
                TextComponent(
                    text='Orgo adalah chatbot yang akan membantu Anda dalam mengatur grup/organisasi ini.\n' + '\n' +
                         'Untuk dapat menggunakan fitur Orgo secara penuh, silakan undang terlebih dahulu Orgo ke grup, ' + \
                         'kemudian daftarkan di sana dengan mengetik /buat_grup', size='sm', wrap=True),
                ButtonComponent(
                    style='link',
                    height='sm',
                    color=WARNA_BIRU,
                    action=MessageAction(label='Butuh Bantuan ?',
                                         text='/bantuan')
                ),
                ButtonComponent(
                    style='link',
                    height='sm',
                    color=WARNA_BIRU,
                    action=MessageAction(label='Menu Utama',
                                         text='/menu')
                )
            ]
        )
    )

    msg =   (
            "Anda dapat pula melakukan dengan kata kunci:\n" + "\n" +
            "/bantuan untuk menuju Pusat Bantuan & Fitur Lain\n" +
            "/menu untuk menuju Menu Utama\n" + '\n' +
            "(Jangan lupa tanda / nya)"
             )

    smart_city_msg = (
'''
===========================
Orgo & Smart City
===========================
Orgo adalah chatbot yang mengusung tema gabungan antara Smart People & Smart Mobility

Nama Orgo diambil dari “ORGanisasi yang terOrganisir”, dibuat \
untuk memberikan kemudahan mengelola dan mengorganisir informasi penting \
dalam grup seperti pengumuman, jadwal, program kerja, file dan dokumen \
agar lebih teratur kapanpun dan dimanapun tanpa dibatasi jarak dan waktu.

Dengan Orgo, setiap anggota grup tidak akan lagi terlewat melihat \
pengumuman, lupa akan jadwal, maupun kesulitan dalam mencari \
file dan dokumen penting dalam organisasi.
'''
    )

    showUI = FlexSendMessage(alt_text="Terima Kasih Telah Mengundang Orgo", contents=UI)
    line_bot_api.reply_message(event.reply_token, [showUI, TextSendMessage(text=msg), TextSendMessage(text=smart_city_msg) ])
