# Modul terjemahan, bisa untuk menerjemahkan API atau lainnya

# Ini menggunakan module mtranslate , installnya:
# pip install mtranslate / pip3 install mtranslate

# Requirements.txt ditambah:
# mtranslate==1.6

# Source code:
# https://github.com/mouuff/mtranslate/

''' Bagian bawah ini mengimport botSetting dan botHelp, tapi dengan cara khusus
hal ini agar file fitur_terjemahan.py tetap dapat di-run dan tidak membuat kebingungan
'''

def kalkulator(equation):
    print("Masuk fungsi kalkulator")
    try:
        hitung = eval(equation)
        hasil = "Hasil perhitungannya adalah:\n" + str(hitung)
    except ZeroDivisionError:
        hasil = "Tidak dapat melakukan pembagian dengan nol"
    except:
        hasil = "Gagal menghitung, Format salah atau hasil terlalu banyak"
    return hasil