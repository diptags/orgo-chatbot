''' Untuk memindahkan URL DP di Line ke server lain (Cloudinary).
Entah kenapa ambil langsung dari line, dia ga dalam bentuk HTTPS
Kalau di rename HTTP jadi HTTPS dia jadi non secure '''

#!/usr/bin/env python
import os, sys
import cloudinary

cloudinary.config(
  cloud_name = 'orgo-bot',  
  api_key = '878188612283167',  
  api_secret = 'XsGaDpH-3qpQYPwaNCnzlK8QxWY'  
)

from cloudinary.uploader import upload
from cloudinary.utils import cloudinary_url
from cloudinary.api import delete_resources_by_tag, resources_by_tag

# config
os.chdir(os.path.join(os.path.dirname(sys.argv[0]), '.'))
if os.path.exists('settings.py'):
    exec(open('settings.py').read())

DEFAULT_TAG = "orgo-bot-upload"

def dump_response(response):
    print("Upload response:")
    for key in sorted(response.keys()):
        print("  %s: %s" % (key, response[key]))

def upload_image(img_link, user_id):

    print("--- Fetch an uploaded remote image, fitting it into 500x500 and reducing saturation")
    
    response = upload(img_link, public_id = user_id )

    dump_response(response)
    url, options = cloudinary_url(response['public_id'],
        format = response['format'],
    )
    return 'https://' + str(url)[7:]