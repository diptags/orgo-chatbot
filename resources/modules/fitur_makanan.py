import sys
import os.path

import linebot
from linebot.models import SourceRoom, SourceGroup

from fatsecret import Fatsecret
from mtranslate import translate

from resources.setup import line_bot_api

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")

from linebot.models import TemplateSendMessage, TextSendMessage, CarouselTemplate, CarouselColumn, URITemplateAction

def olahTerjemahan(kalimatYangMauDiterjemahkan, kodeBahasaTujuan='id'):

    hasilTerjemahan = translate(kalimatYangMauDiterjemahkan, kodeBahasaTujuan)
    return hasilTerjemahan

def tampilHasilGizi(eventInteraksi,dataMakanan,namaMakanan):

    listMakanan = list(dataMakanan.keys())
    sajian1 = ((dataMakanan[listMakanan[0]])[0]).split(" - ")
    sajian2 = ((dataMakanan[listMakanan[1]])[0]).split(" - ")

    hasilGizi = TemplateSendMessage(
        alt_text='Hasil pencarian gizi ',
        template=CarouselTemplate(
            columns=[
                CarouselColumn(
                    text= listMakanan[0] + '\n' + '\n' + sajian1[0] + '\n' + sajian1[1] + '\n' + dataMakanan[listMakanan[0]][1] + '\n' + dataMakanan[listMakanan[0]][2] + '\n' + dataMakanan[listMakanan[0]][3],
                    actions=[ URITemplateAction(label='Lihat Selengkapnya', uri= dataMakanan[listMakanan[0]][4] )]),

                CarouselColumn(
                    text= listMakanan[1] + '\n' + '\n' + sajian2[0] + '\n' + sajian2[1] + '\n' + dataMakanan[listMakanan[1]][1] + '\n' + dataMakanan[listMakanan[1]][2] + '\n' + dataMakanan[listMakanan[1]][3],
                    actions=[ URITemplateAction(label='Lihat Selengkapnya', uri= dataMakanan[listMakanan[1]][4] )]),
            ]))

    try:
        if isinstance(eventInteraksi, SourceGroup) or isinstance(eventInteraksi, SourceRoom):
            line_bot_api.reply_message(eventInteraksi.reply_token, TextSendMessage(text="Tolong periksa personal chat Anda"))
        line_bot_api.push_message(eventInteraksi.source.user_id, [hasilGizi, TextSendMessage(text="Berikut kandungan gizi pada " + namaMakanan + " \n" + "Jangan lupa makan tepat waktu, olahraga teratur dan tidur yang cukup!")])
    except linebot.exceptions.LineBotApiError:
        line_bot_api.reply_message(eventInteraksi.reply_token, TextSendMessage(text="Mohon tambahkan Orgo sebagai teman dulu ya"))

def olahMakanan(foodName):
    fs = Fatsecret("bb5a9b1163fb489e8d262acc0408bc3d", "b1045f13406a4101b6e7df448fbbf0eb")
    foods = fs.foods_search(foodName)

    daftarMakanan = {}

    for i in range (0,2):
        dataMakanan = foods[i]
        for key in dataMakanan:
            if key == 'food_description':
                hasilTerjemah = (olahTerjemahan(dataMakanan['food_description'],'id').split(" | "))
                namaMakanan = olahTerjemahan(dataMakanan['food_name'],'id')
                linkMakanan = dataMakanan['food_url']
                hasilTerjemah.append(linkMakanan)

        daftarMakanan[namaMakanan] = (hasilTerjemah)

    return daftarMakanan

def get_makanan(eventInteraksi, foodName):
    namaMakananInggris = olahTerjemahan(foodName,'en')

    try:
        dataMakanan = olahMakanan(namaMakananInggris)
    except:
        line_bot_api.reply_message(eventInteraksi.reply_token, TextSendMessage(text="Mohon maaf, pencarian gizi gagal! Silakan coba kembali dengan mengganti hidangan dan periksa kembali koneksi internet Anda"))
    else:
        tampilHasilGizi(eventInteraksi,dataMakanan,foodName)
