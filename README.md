# README

Chat Bot Manajemen Organisasi adalah chat bot yang memudahkan dan mengoptimalkan berjalannya organisasi.

## Fitur
  
Checklist yang sudah

- [X] Dapat memberi jabatan
- [ ] Dapat membuat divisi/ biro/ team
- [X] Dapat membuat objek proker
- [ ] Dapat membuat proker dalam proker (subproker)
- [ ] Fitur penilaian proker (optional)
- [X] Pengumuman organisasi
- [X] Jadwal seluruh orgnisasi
- [ ] Dapat membuat agenda/tugas baik didalam maupun diluar proker (bentuknya progres)
- [ ] Dapat memberitahu agenda yang akan datang atau deadline tugas dan bisa disync ke gmail jika ada
- [ ] Atasan dapat menambahkan tugas ke bawahan
- [ ] bawahan dapat menceklis tugasnya jika sudah selesai
- [ ] Dapat membuat LPJ baik dalam chat maupun download Template LPJ nya terus edit sendiri di word
- [ ] Bawahan dapat submit LPJ dan atasan bisa lihat
- [ ] Dapat membuat graf struktur/hirarki organisasi
- [ ] Kalau ada 1 orang dengan banyak organisasi, dapet notifikasi dari semuanya
- [ ] Cloud document storage
- [ ] Auto Document generator (surat izin, dll)

- [ ] Fitur penilaian fast response (optional)
- [ ] Fitur angket dimana angket harus dijawab oleh semua anggota (yang ditargetkan) dan yg ngajuin angket bsia nelist siapa aja yang belum jawa angket dan bisa mengingatkan lewat bot line
- [ ] Dapat membuat absensi online (menggunakan QR code mungkin?) (optional)
- [X] Fitur pendapat anonymous (optional)
- [X] Member profile
- [X] Member database
- [ ] member task centang
- [ ] calendar/timeline
- [ ] rsvp
- [ ] repeat jadwal
- [X] konfirmasi melihat pengumuman 
- [ ] repository(optional)
- [ ] excel integration (optional)
- [ ] diagram (misal siapa yang paling banyak chat) (optional)
- [ ] Informasi per orang (hasil cek kesehatan, record gajian) dari csv
- [ ] Fitur penilaina karyawan/staff (optional)
- [X] Informasi karyawan (skillset dll)  

- [ ] Fitur voting (optional) ** Sepertinya sudah ada di LINE **
- [ ] Fitur join grup pake QR Code atau Token ** Sepertinya sudah ada di LINE **

  
# Dikembangkan oleh:  
Tim Maba Palsu  
1. Aab (Muhamad Abdurahman)  
2. Dipsi  (Rd Pradipta Gitaya S)
3. Sidiq (Usman Sidiq)
