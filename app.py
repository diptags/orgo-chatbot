"""
Main program

Setiap interaksi yang terjadi di bot, dianggap sebagai sebuah event
Setiap event mempunya token aktivitasnya sendiri - sendiri sebagai identitas dari event itu,
tapi tak perlu dipikirkan karena itu otomatis

Bagian bawah ini menghandle pesan masuk jika bot menerima dalam bentuk
pesan TEKS (message=TextMessage)

"MessageEvent" maksudnya bisa dibilang adalah tujuan module nya,
terus "TextMessage" itu kalau misalnya bot menerima pesan berupa TEKS

Pesan yang diterima bot kan bisa bermacam - macam, bisa berupa text, berupa gambar,
berupa stiker, dan lain - lainnya

Nama def/function nya, sudah ada dari sananya
"""

import os

from linebot.models import TextMessage, MessageEvent, LocationMessage, PostbackEvent, FileMessage, \
    FollowEvent, JoinEvent, UnfollowEvent, LeaveEvent

from resources.base_decorator import per_session_basic_setup
from resources.event_handler.file_message_handler import handle_file_message
from resources.event_handler.location_message_handler import handle_loc_message
from resources.event_handler.message_text_handler import handle_text_message
from resources.event_handler.postback_handler import handle_postback
from resources.modules.user_interface import selamat_datang_grup, selamat_datang_nongrup
from resources.schedule import start_schedule
from resources.setup import handler, app

handle_text_message = per_session_basic_setup(handle_text_message)
handle_text_message = handler.add(MessageEvent, message=TextMessage)(handle_text_message)

handle_loc_message = per_session_basic_setup(handle_loc_message)
handle_loc_message = handler.add(MessageEvent, message=LocationMessage)(handle_loc_message)

handle_postback = per_session_basic_setup(handle_postback)
handle_postback = handler.add(PostbackEvent)(handle_postback)

handle_file_message = per_session_basic_setup(handle_file_message)
handle_file_message = handler.add(MessageEvent, message=FileMessage)(handle_file_message)


@handler.add(FollowEvent)
def handle_follow(event):
    selamat_datang_nongrup(event)


@handler.add(JoinEvent)
def handle_join(event):
    selamat_datang_grup(event)


@handler.add(UnfollowEvent)
def handle_unfollow():
    app.logger.info("Seseorang mengunfollow atau blok Orgo")


@handler.add(LeaveEvent)
def handle_leave():
    app.logger.info("Seseorang menghapus Orgo dari grup")


if __name__ == "__main__":
    start_schedule()
    PORT = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=PORT)
